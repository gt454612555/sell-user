package sell.user.bootstrap;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>
 *  springboot启动器
 * </p>
 *
 * @author gt
 * @date 2021/1/7
 */

@Slf4j
@ComponentScan({"sell.user","sell.common"})
@MapperScan({"sell.user.**.mapper","sell.common.**.mapper"})
@DubboComponentScan({"sell.user","sell.common"})
@SpringBootApplication
@EnableTransactionManagement
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

    /**
     * 加载应用名称用于监控
     * @param applicationName 应用名称
     * @return 监控注册
     */
    @Bean
    MeterRegistryCustomizer<MeterRegistry> customizer(@Value("${spring.application.name}") String applicationName){
        return (registry) -> registry.config().commonTags("application",applicationName);
    }
}
