package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sell_customer_cart")
@ApiModel(value = "用户购物车关联表PO")
public class CustomerCartPO extends BasePO {

    /**
    * 顾客用户id
    * isNullAble:0
    */
    private Long uid;

    /**
    * 商品id
    * isNullAble:0
    */
    private Long goodsId;

    /**
    * 选择数量
    * isNullAble:0,defaultVal:1
    */
    private Integer buyNum;

}
