package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@TableName(value = "sell_role")
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "角色表")
public class RolePO extends BasePO {

    /**
     * 角色名
     */
    private String name;
    /**
     * 状态 1 启用 0 禁用
     */
    private Boolean status;


}
