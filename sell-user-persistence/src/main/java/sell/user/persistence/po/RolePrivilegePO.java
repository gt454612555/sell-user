package sell.user.persistence.po;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;

/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("角色权限表")
@TableName("sell_role_privilege")
public class RolePrivilegePO extends BasePO {
    /**
    * 角色id(1l:顾客  2L:商家  3L:管理员)
    * isNullAble:0
    */
    private Long roleId;

    /**
    * 权限id 对应path的主键
    * isNullAble:0
    */
    private Long privilegeId;

    /**
    * 1 启用 0 禁用
    * isNullAble:0,defaultVal:1
    */
    private Boolean status;


}
