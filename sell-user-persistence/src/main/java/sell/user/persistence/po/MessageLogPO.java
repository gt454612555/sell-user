package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;
import sell.common.enums.message.BroadcastObjEnum;
import sell.common.enums.message.MessageTypeEnum;
import sell.common.enums.message.SendTypeEnum;

/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("消息记录表")
@TableName("sell_message_log")
public class MessageLogPO extends BasePO {



    /**
    * 消息内容
    * isNullAble:0
    */
    private String content;

    /**
    * 接收人uid
    * isNullAble:1
    */
    private Long recUid;

    /**
    * 消息类型：1=>订单通知，2=>账户通知，3=>商品通知，
4=>提现审核 5=>商品审核 6=>系统公告

    * isNullAble:1
    */
    private MessageTypeEnum type;

    /**
    * 状态 1=>已读 0=>未读
    * isNullAble:0,defaultVal:0
    */
    private Boolean isRead;

    /**
    * 消息发送类型：1=广播，2=单播
    * isNullAble:1,defaultVal:2
    */
    private SendTypeEnum sendType;

    /**
    * 广播对象：1-顾客 2-商家 3-管理员 4-全部
    * isNullAble:1,defaultVal:4
    */
    private BroadcastObjEnum broadcastObj;


}
