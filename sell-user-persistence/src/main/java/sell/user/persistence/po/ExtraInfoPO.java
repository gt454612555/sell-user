package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;

/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@TableName(value = "sell_extra_info")
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户其他信息表")
public class ExtraInfoPO extends BasePO {
    /**
    * 真实姓名
    * isNullAble:1
    */
    private String realName;

    /**
     * 实体名称
     */
    private String entityName;

    /**
    * 邮箱
    * isNullAble:1
    */
    private String email;

    /**
    * 用户id
    * isNullAble:0
    */
    private Long uid;

    /**
    * 年龄
    * isNullAble:1
    */
    private Integer age;

    /**
    * 支付宝账户
    * isNullAble:1
    */
    private String aliPay;

    private Long avatar;
}
