package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;


/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("消息模板表")
@TableName("sell_message_template")
public class MessageTemplatePO extends BasePO {

    /**
    * 模版编码
    * isNullAble:1
    */
    private String code;

    /**
    * 模板名称
    * isNullAble:1
    */
    private String name;

    /**
    * 模版内容
    * isNullAble:0
    */
    private String content;

}
