package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import sell.common.dal.po.BasePO;

/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户角色表")
@TableName("sell_user_role")
public class UserRolePO extends BasePO {

    /**
    * 用户id
    * isNullAble:1
    */
    @ApiModelProperty(value = "用户id")
    private Long uid;

    /**
    * 角色id
    * isNullAble:1
    */
    @ApiModelProperty(value = "角色id 1-顾客 2-商家 3-管理员")
    private Long roleId;

}
