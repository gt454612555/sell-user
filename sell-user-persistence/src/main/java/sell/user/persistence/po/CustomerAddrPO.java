package sell.user.persistence.po;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.*;
import sell.common.dal.po.BasePO;

/**
*
*  @author author
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "sell_customer_addr")
@ApiModel(value = "顾客用户收货信息表")
public class CustomerAddrPO extends BasePO {

    /**
    * 用户id
    * isNullAble:0
    */
    private Long uid;

    /**
    * 省编码
    * isNullAble:1
    */
    private String provinceCode;

    /**
    * 省名称
    * isNullAble:1
    */
    private String provinceName;

    /**
    * 市编码
    * isNullAble:1
    */
    private String cityCode;

    /**
    * 市名称
    * isNullAble:1
    */
    private String cityName;

    /**
    * 区编码
    * isNullAble:1
    */
    private String areaCode;

    /**
    * 区名称
    * isNullAble:1
    */
    private String areaName;

    /**
    * 详细地址
    * isNullAble:1
    */
    private String address;

    /**
    * 收货人手机号
    * isNullAble:1
    */
    private String userMobile;

    /**
    * 收货人姓名
    * isNullAble:1
    */
    private String userName;

    /**
    * 排序
    * isNullAble:1,defaultVal:0
    */
    private Integer sort;

    /**
    * 是否为默认地址 0-否 1-是
    * isNullAble:1,defaultVal:0
    */
    private Boolean isDefault;
}
