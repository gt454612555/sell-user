package sell.user.persistence.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/24
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户消息已读表")
public class UserMessageReadPO {

    @ApiModelProperty(value = "用户id")
    private Long uid;

    @ApiModelProperty(value = "消息id")
    private Long messageId;

}
