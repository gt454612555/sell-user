package sell.user.persistence.iservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import sell.user.persistence.mapper.RolePrivilegeMapper;
import sell.user.persistence.po.RolePrivilegePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
@Repository
public class RolePrivilegeIServiceImpl extends ServiceImpl<RolePrivilegeMapper, RolePrivilegePO> implements IService<RolePrivilegePO> {
}
