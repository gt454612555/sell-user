package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import sell.user.persistence.po.CustomerAddrPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
public interface CustomerAddrMapper extends BaseMapper<CustomerAddrPO> {
}
