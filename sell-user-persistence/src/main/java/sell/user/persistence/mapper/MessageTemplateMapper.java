package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import sell.user.persistence.po.MessageTemplatePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
public interface MessageTemplateMapper extends BaseMapper<MessageTemplatePO> {
}
