package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sell.user.persistence.dto.res.RolePrivilegeResDTO;
import sell.user.persistence.po.RolePrivilegePO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
public interface RolePrivilegeMapper extends BaseMapper<RolePrivilegePO> {

    @Delete("DELETE FROM sell_role_privilege WHERE privilege_id = #{privilegeId}")
    Integer deleteUrlRoles(@Param("privilegeId") Long privilegeId);

    @Select("select t1.role_id,t2.id,t2.name,t2.description from sell_role_privilege t1 left join sell_privilege t2 on t1.privilege_id = t2.id and t2.status = 1 where t1.delete_flag = 0 and t1.status = 1 and t1.role_id = #{roleId}")
    List<RolePrivilegeResDTO> getPrivilegeByRole(@Param("roleId") Integer roleId);

    @Select("select privilege_id from sell_role_privilege where delete_flag = 0 and status = 1 and role_id = #{roleId}")
    List<Long> getSavedPrivilegeIdsByRole(@Param("roleId")Integer roleId);
}
