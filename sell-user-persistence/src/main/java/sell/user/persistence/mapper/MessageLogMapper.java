package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import sell.user.persistence.po.MessageLogPO;
import sell.user.persistence.po.UserMessageReadPO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
public interface MessageLogMapper extends BaseMapper<MessageLogPO> {
    /**
     * 消息已读
     * @param messageId 消息id
     * @param uid 用户id
     */
    @Insert("insert into sell_user_message_read (message_id,uid) VALUES (#{messageId},#{uid})")
    void insertIntoMsgRead(@Param("messageId")Long messageId,@Param("uid")Long uid);

    void insertBatchMsgRead(@Param("list")List<UserMessageReadPO> list);

    /**
     * 查询是否已读
     * @param messageId
     * @param uid
     * @return
     */
    @Select("select count(*) from sell_user_message_read where message_id = #{messageId} and uid = #{uid}")
    Integer getReadNum(@Param("messageId")Long messageId,@Param("uid")Long uid);
}
