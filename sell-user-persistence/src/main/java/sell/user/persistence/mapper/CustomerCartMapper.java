package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import sell.user.persistence.po.CustomerCartPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/3/13
 */
public interface CustomerCartMapper extends BaseMapper<CustomerCartPO> {
}
