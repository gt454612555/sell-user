package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import sell.user.persistence.dto.req.AdminListSearchDTO;
import sell.user.persistence.dto.res.AdminListResDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface AdminListQueryMapper extends BaseMapper<AdminListResDTO> {

    List<AdminListResDTO> getAdminList(@Param("search")AdminListSearchDTO search);

}
