package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import sell.user.persistence.dto.req.UserSearchDTO;
import sell.user.persistence.po.UserPO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
public interface UserMapper extends BaseMapper<UserPO> {

    List<UserPO> getCustomerPage(@Param("search")UserSearchDTO search);

    List<UserPO> getSellerPage(@Param("search")UserSearchDTO search);

}
