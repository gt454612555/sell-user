package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import sell.user.persistence.dto.req.RoleSearchDTO;
import sell.user.persistence.po.RolePO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface RoleMapper  extends BaseMapper<RolePO> {

    List<RolePO> getRolePageList(@Param("search")RoleSearchDTO search);

}
