package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import sell.common.enums.message.BroadcastObjEnum;
import sell.user.persistence.dto.req.MessageSelectDTO;
import sell.user.persistence.dto.res.MessagePageResDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
public interface MessageQueryMapper extends BaseMapper<MessagePageResDTO> {
    /**
     * 分页消息列表
     * @param search
     * @param uid
     * @param broadcastType
     * @return
     */
    List<MessagePageResDTO> getPageList(@Param("search")MessageSelectDTO search, @Param("uid")Long uid, @Param("broadcastType")BroadcastObjEnum broadcastType);

    /**
     * 获取未读消息数量
     * @param uid
     * @param broadcastType
     * @return
     */
    Integer getUnReadMsgNum(@Param("uid")Long uid, @Param("broadcastType")BroadcastObjEnum broadcastType);

    /**
     * 获取未读消息的id
     * @param uid
     * @param broadcastType
     * @return
     */
    List<Long> getUnReadMsgId(@Param("uid")Long uid, @Param("broadcastType")BroadcastObjEnum broadcastType);


    List<MessagePageResDTO> getAppPage(@Param("query")Page<MessagePageResDTO> query,@Param("uid")Long uid);
}
