package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import sell.user.persistence.po.ExtraInfoPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
public interface ExtraInfoMapper extends BaseMapper<ExtraInfoPO> {
}
