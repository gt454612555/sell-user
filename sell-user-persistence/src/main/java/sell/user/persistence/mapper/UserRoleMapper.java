package sell.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import sell.user.persistence.po.UserRolePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
public interface UserRoleMapper extends BaseMapper<UserRolePO> {
}
