package sell.user.persistence.repository.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import sell.common.api.rpc.AccountRpcFacade;
import sell.common.enums.user.UserTypeEnum;
import sell.common.exception.ServiceException;
import sell.common.security.context.UserContextHolder;
import sell.user.persistence.iservice.RolePrivilegeIServiceImpl;
import sell.user.persistence.mapper.*;
import sell.user.persistence.po.*;
import sell.user.persistence.repository.UserRepository;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class UserRepositoryImpl implements UserRepository {

    @Reference(check = false)
    private AccountRpcFacade accountRpcFacade;

    private final UserMapper userMapper;
    private final UserRoleMapper userRoleMapper;
    private final ExtraInfoMapper extraInfoMapper;
    private final CustomerAddrMapper customerAddrMapper;
    private final CustomerCartMapper customerCartMapper;


    @Override
    public Map<String,Boolean> hasSameNickNameAndMobile(String nickName, String mobile) {
        Map<String,Boolean> res = new HashMap<>(2);
        LambdaQueryWrapper<UserPO> nickNameWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<UserPO> mobileWrapper = new LambdaQueryWrapper<>();
        nickNameWrapper.eq(UserPO::getNickName,nickName);
        mobileWrapper.eq(UserPO::getMobile,mobile);
        List<UserPO> sameNickNameList = userMapper.selectList(nickNameWrapper);
        List<UserPO> sameMobileList = userMapper.selectList(mobileWrapper);
        res.put("nickName",CollectionUtils.isNotEmpty(sameNickNameList));
        res.put("mobile",CollectionUtils.isNotEmpty(sameMobileList));
        return res;
    }

    @Override
    public Long insertIntoUserAndRole(UserPO po) {
        long userTableKey = IdWorker.getId();
        po.setId(userTableKey);
        //插入user表
        userMapper.insert(po);
        //插入user_role表
        UserRolePO userRolePo = UserRolePO.builder().uid(userTableKey).roleId(po.getRole().getValue().longValue()).build();
        userRoleMapper.insert(userRolePo);
        return userTableKey;
    }

    @Override
    public String getMobileByUid(Long uid) {
        UserPO po = userMapper.selectById(uid);
        return po.getMobile();
    }

    @Override
    public UserPO getUserById(Long uid) {
        return userMapper.selectById(uid);
    }



    @Override
    public void updateUserInfo(UserPO user) {
        if (user==null || user.getId()==null){
            log.info("UserRepositoryImpl#updateUserInfo=>更新用户信息，入参的用户/用户id为空！");
            throw new ServiceException("用户信息更新失败，请联系管理员！");
        }
        userMapper.updateById(user);
    }

    @Override
    public ExtraInfoPO getExtraInfo(Long uid) {
        LambdaQueryWrapper<ExtraInfoPO> wrapper = Wrappers.<ExtraInfoPO>lambdaQuery().eq(ExtraInfoPO::getUid, uid);
        return extraInfoMapper.selectOne(wrapper);
    }

    @Override
    public Long insertExtraInfo(ExtraInfoPO po) {
        long id = IdWorker.getId();
        po.setId(id);
        extraInfoMapper.insert(po);
        return id;
    }

    @Override
    public void updateAvatar(Long avatar) {
        Long userId = UserContextHolder.getUserId();
        new LambdaUpdateChainWrapper<>(extraInfoMapper).eq(ExtraInfoPO::getUid,userId).set(ExtraInfoPO::getAvatar,avatar).update();
    }

    @Override
    public Boolean updateExtraInfo(ExtraInfoPO po) {
        LambdaQueryWrapper<ExtraInfoPO> wrapper = Wrappers.<ExtraInfoPO>lambdaQuery().eq(ExtraInfoPO::getUid, po.getUid());
        return extraInfoMapper.update(po,wrapper)==1;
    }

    @Override
    public CustomerAddrPO getDefaultAddr(Long uid) {
        LambdaQueryWrapper<CustomerAddrPO> wrapper = Wrappers.<CustomerAddrPO>lambdaQuery().eq(CustomerAddrPO::getUid, uid).eq(CustomerAddrPO::getIsDefault, Boolean.TRUE);
        return customerAddrMapper.selectOne(wrapper);
    }

    @Override
    public CustomerAddrPO getAddrById(Long addrId) {
        return customerAddrMapper.selectById(addrId);
    }

    @Override
    public Long insertCustomerAddr(CustomerAddrPO po) {
        long id = IdWorker.getId();
        po.setId(id);
        customerAddrMapper.insert(po);
        return id;
    }

    @Override
    public Boolean deleteAddr(Long addrId) {
        return customerAddrMapper.deleteById(addrId) == 1;
    }

    @Override
    public List<CustomerAddrPO> getAddrList(Long uid) {
        LambdaQueryWrapper<CustomerAddrPO> wrapper = Wrappers.lambdaQuery(CustomerAddrPO.class).eq(CustomerAddrPO::getUid, uid);
        return customerAddrMapper.selectList(wrapper);
    }

    @Override
    public CustomerAddrPO getAddrInfo(@NotNull Long uid, @NotNull Long addrId) {
        CustomerAddrPO customerAddrPO = customerAddrMapper.selectById(addrId);
        Assert.notNull(customerAddrPO,"此收货地址信息不存在");
        if (!uid.equals(customerAddrPO.getUid())){
            throw new ServiceException("您没有查看他人收货地址信息的权限！");
        }
        return customerAddrPO;
    }

    @Override
    public Long addGoodsIntoCart(@NotNull Long uid, @NotNull Long goodsId, @NotNull @Min(1L) Integer buyNum) {
        CustomerCartPO cartPO = CustomerCartPO.builder()
                .buyNum(buyNum)
                .goodsId(goodsId)
                .uid(uid)
                .build();
        cartPO.setId(IdWorker.getId());
        customerCartMapper.insert(cartPO);
        return cartPO.getId();
    }

    @Override
    public CustomerCartPO getCartInfoById(Long cartId) {
        return customerCartMapper.selectById(cartId);
    }

    @Override
    public Integer getCartNum(Long uid) {
        List<CustomerCartPO> customerCartPOS = customerCartMapper.selectList(Wrappers.<CustomerCartPO>lambdaQuery().eq(CustomerCartPO::getUid, uid));
        return CollectionUtils.isEmpty(customerCartPOS)? 0 :customerCartPOS.size();
    }

    @Override
    public void deleteCart(Long cartId) {
        customerCartMapper.deleteById(cartId);
    }

    @Override
    public void deleteCart(List<Long> cartIds) {
        customerCartMapper.deleteBatchIds(cartIds);
    }

    @Override
    public void deleteCartByUid(Long uid) {
        customerCartMapper.delete(Wrappers.<CustomerCartPO>lambdaQuery().eq(CustomerCartPO::getUid,uid));
    }

    @Override
    public void changeCartNum(Long id, Integer num) {
        CustomerCartPO customerCartPO = customerCartMapper.selectById(id);
        Assert.notNull(customerCartPO,"购物车信息不存在");
        if (!UserContextHolder.getUserId().equals(customerCartPO.getUid())) {
            throw new ServiceException("您无权操作他人的购物车信息");
        }
        new LambdaUpdateChainWrapper<>(customerCartMapper).eq(CustomerCartPO::getId,id).set(CustomerCartPO::getBuyNum,num).update();
    }

    @Override
    public void setDefaultAddrId(Long oldDefault, Long newDefault) {
        //取消原来的默认设置
        new LambdaUpdateChainWrapper<>(customerAddrMapper).eq(CustomerAddrPO::getId,oldDefault).set(CustomerAddrPO::getIsDefault,Boolean.FALSE).update();
        //新默认设置
        new LambdaUpdateChainWrapper<>(customerAddrMapper).eq(CustomerAddrPO::getId,newDefault).set(CustomerAddrPO::getIsDefault,Boolean.TRUE).update();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteUser(Long id) {

        if (UserContextHolder.getUserId().equals(id)) {
            throw new ServiceException("无法删除本人的账号!");
        }
        userMapper.deleteById(id);
        userRoleMapper.delete(Wrappers.<UserRolePO>lambdaQuery().eq(UserRolePO::getUid,id));
        extraInfoMapper.delete(Wrappers.<ExtraInfoPO>lambdaQuery().eq(ExtraInfoPO::getUid,id));
        accountRpcFacade.deleteAccount(id);

    }

}
