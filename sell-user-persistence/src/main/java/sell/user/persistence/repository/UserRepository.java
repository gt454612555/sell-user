package sell.user.persistence.repository;

import org.springframework.validation.annotation.Validated;
import sell.common.enums.user.UserTypeEnum;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.CustomerCartPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.po.UserPO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Validated
public interface UserRepository {


    /**
     * 查询是否有相同的手机号和电话号码
     * @param nickName 昵称
     * @param mobile 手机号
     * @return 一个Map ,key:{nickName,mobile},value 代表是否重复
     */
    Map<String,Boolean> hasSameNickNameAndMobile(String nickName,String mobile);

    /**
     * 注册用户向user表和userRole表插入数据
     * @param po 用户对象
     * @return 用户主键id
     */
    Long insertIntoUserAndRole(UserPO po);

    /**
     * 获取用户手机号
     * @param uid 用户id
     * @return 手机号 测试专用，其实可以直接从ThreadLocal中获取
     */
    String getMobileByUid(Long uid);

    /**
     * 根据用户id获取用户信息
     * @param uid 用户id
     * @return 用户对象
     */
    UserPO getUserById(Long uid);



    /**
     * 更新user表数据（根据uid）
     * @param user 用户
     */
    void updateUserInfo(UserPO user);

    /**
     * 获取用户的其他信息
     * @param uid
     * @return
     */
    ExtraInfoPO getExtraInfo(Long uid);

    /**
     * 新增顾客用户的个人信息
     * @param po
     * @return
     */
    Long insertExtraInfo(ExtraInfoPO po);

    /**
     * 修改头像
     * @param avatar
     */
    void updateAvatar(Long avatar);

    /**
     * 修改用户的个人信息
     * @param po
     * @return
     */
    Boolean updateExtraInfo(ExtraInfoPO po);

    /**
     * 查询用户的默认收货地址
     * @param uid
     * @return
     */
    CustomerAddrPO getDefaultAddr(Long uid);

    /**
     * 根据主键查询收货地址
     * @param addrId
     * @return
     */
    CustomerAddrPO getAddrById(Long addrId);

    /**
     * 新增一条收货地址，并返回主键
     * @param po
     * @return
     */
    Long insertCustomerAddr(CustomerAddrPO po);

    /**
     * 根据主键删除一条收货地址
     * @param addrId
     * @return
     */
    Boolean deleteAddr(Long addrId);

    /**
     * 获取顾客用户的所有收货地址列表
     * @param uid
     * @return
     */
    List<CustomerAddrPO> getAddrList(Long uid);

    /**
     * 获取收货地址信息+数据权限校验
     * @param uid
     * @param addrId
     * @return
     */
    CustomerAddrPO getAddrInfo(@Validated @NotNull Long uid, @Validated @NotNull Long addrId);

    /**
     * 添加商品到购物车
     * @param uid 顾客用户id
     * @param goodsId 商品id
     * @param buyNum 购买数量
     * @return 购物车记录id
     */
    Long addGoodsIntoCart(@Validated @NotNull Long uid, @Validated @NotNull Long goodsId, @Validated @NotNull @Min(1L) Integer buyNum);

    /**
     * 查询购物车信息
     * @param cartId
     * @return
     */
    CustomerCartPO getCartInfoById(Long cartId);

    /**
     * 查询用户的购物车商品数量
     * @param uid
     * @return
     */
    Integer getCartNum(Long uid);

    /**
     * 删除购物车信息
     * @param cartId
     */
    void deleteCart(Long cartId);

    /**
     * 批量删除购物车信息
     * @param cartIds
     */
    void deleteCart(List<Long> cartIds);

    /**
     * 删除一个人的所有购物车信息
     * @param uid
     */
    void deleteCartByUid(Long uid);

    /**
     * 修改购物车的购买数量
     * @param id
     * @param num
     */
    void changeCartNum(Long id,Integer num);

    /**
     * 设置默认收货地址
     * @param oldDefault
     * @param newDefault
     */
    void setDefaultAddrId(Long oldDefault,Long newDefault);

    void deleteUser(Long id);
}
