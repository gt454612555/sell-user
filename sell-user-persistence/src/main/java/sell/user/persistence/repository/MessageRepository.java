package sell.user.persistence.repository;

import org.springframework.validation.annotation.Validated;
import sell.common.api.dto.SendMsgDTO;
import sell.common.enums.message.BroadcastObjEnum;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Validated
public interface MessageRepository {
    /**
     *
     * @param dto
     */
    Long sendMsg(@Validated @Valid SendMsgDTO dto);

    /**
     * 发送系统广播公告消息
     * @param content 消息内容
     * @param broadcastObjEnum 广播对象
     * @return 消息id
     */
    Long sendSysMsg(String content, BroadcastObjEnum broadcastObjEnum);

    /**
     * 已读一条消息
     * @param msgId
     */
    void readMsg(Long msgId);

    void readAll();

}
