package sell.user.persistence.repository;

import sell.common.enums.user.SexTypeEnum;
import sell.user.persistence.po.UserPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface AdminRepository {

    Boolean changeIsLock(Boolean isLock,Long id);

    Long addAdmin(UserPO user);

    void deleteAdmin(Long id);

    void updateAdminBaseInfo(Long id, String mobile, String nickName, SexTypeEnum sex);
}
