package sell.user.persistence.repository.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import sell.common.api.dto.SendMsgDTO;
import sell.common.enums.message.BroadcastObjEnum;
import sell.common.enums.message.MessageTypeEnum;
import sell.common.enums.message.SendTypeEnum;
import sell.common.enums.user.UserTypeEnum;
import sell.common.exception.ServiceException;
import sell.common.security.context.UserContextHolder;
import sell.common.security.dto.UserInfo;
import sell.common.utils.ObjectUtils;
import sell.user.persistence.mapper.MessageLogMapper;
import sell.user.persistence.mapper.MessageQueryMapper;
import sell.user.persistence.mapper.MessageTemplateMapper;
import sell.user.persistence.po.MessageLogPO;
import sell.user.persistence.po.MessageTemplatePO;
import sell.user.persistence.po.UserMessageReadPO;
import sell.user.persistence.repository.MessageRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class MessageRepositoryImpl implements MessageRepository {

    private final MessageTemplateMapper messageTemplateMapper;
    private final MessageLogMapper messageLogMapper;
    private final MessageQueryMapper messageQueryMapper;

    @Override
    public Long sendMsg(@Valid SendMsgDTO dto) {
        Assert.notNull(dto,"消息发送参数缺失");
        if (SendTypeEnum.BROADCAST.equals(dto.getSendType())){
            //广播
            Assert.notNull(dto.getBroadcastObj(),"消息发送广播对象缺失");
        }else {
            //单播
            Assert.notNull(dto.getRecUid(),"消息发送单播接收者缺失");
        }
        //获取模板
        MessageTemplatePO templatePO = messageTemplateMapper.selectOne(Wrappers.<MessageTemplatePO>lambdaQuery().eq(MessageTemplatePO::getCode, dto.getMessageTemplate().getTemplateCode()));
        Assert.notNull(templatePO,"消息模板有误");
        String messageContent = "";
        try {
            messageContent = ObjectUtils.contentFormat(templatePO.getContent(), dto.getParams());
        } catch (Exception e) {
            log.error("消息模板填充出错",e);
            return null;
        }
        MessageLogPO messageLogPO = MessageLogPO.builder()
                .broadcastObj(dto.getBroadcastObj())
                .content(messageContent)
                .isRead(Boolean.FALSE)
                .recUid(dto.getRecUid())
                .sendType(dto.getSendType())
                .type(dto.getType())
                .build();
        messageLogPO.setId(IdWorker.getId());
        messageLogMapper.insert(messageLogPO);
        return messageLogPO.getId();
    }

    @Override
    public Long sendSysMsg(String content, BroadcastObjEnum broadcastObjEnum) {
        long msgId = IdWorker.getId();
        MessageLogPO messageLogPO = MessageLogPO.builder()
                .type(MessageTypeEnum.SYSTEM)
                .sendType(SendTypeEnum.BROADCAST)
                .isRead(Boolean.FALSE)
                .content(content)
                .broadcastObj(broadcastObjEnum)
                .build();
        messageLogPO.setId(msgId);
        messageLogMapper.insert(messageLogPO);
        return msgId;
    }

    @Override
    public void readMsg(Long msgId) {
        UserInfo userInfo = UserContextHolder.get();
        MessageLogPO messageLogPO = messageLogMapper.selectById(msgId);
        Assert.notNull(messageLogPO,"消息不存在");
        SendTypeEnum sendType = messageLogPO.getSendType();
        if (SendTypeEnum.BROADCAST.equals(sendType)){
            //广播消息
            BroadcastObjEnum broadcastObj = messageLogPO.getBroadcastObj();
            if (!BroadcastObjEnum.ALL.equals(broadcastObj)){
                //不是面向全体用户的广播
                if (!UserTypeEnum.fromValue(broadcastObj.getValue()).equals(userInfo.getRole())){
                    throw new ServiceException("您无权阅读不属于您的消息");
                }
            }
        }else {
            //单播
            if (!userInfo.getId().equals(messageLogPO.getRecUid())){
                throw new ServiceException("您无权阅读不属于您的消息");
            }
        }
        //消息已读
        Integer readNum = messageLogMapper.getReadNum(msgId, userInfo.getId());
        if (readNum > 0 ){
            return;
        }
        messageLogMapper.insertIntoMsgRead(msgId,userInfo.getId());
    }

    @Override
    public void readAll() {
        UserInfo userInfo = UserContextHolder.get();
        List<Long> unReadMsgId = messageQueryMapper.getUnReadMsgId(userInfo.getId(), BroadcastObjEnum.fromValue(userInfo.getRole().getValue()));

        if (CollectionUtils.isNotEmpty(unReadMsgId)){
            List<UserMessageReadPO> readPOS = unReadMsgId.stream().map(id -> UserMessageReadPO.builder().messageId(id).uid(userInfo.getId()).build()).collect(Collectors.toList());
            messageLogMapper.insertBatchMsgRead(readPOS);
        }
    }
}
