package sell.user.persistence.repository.impl;

import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sell.common.api.rpc.AccountRpcFacade;
import sell.common.enums.user.SexTypeEnum;
import sell.common.enums.user.UserTypeEnum;
import sell.common.exception.ServiceException;
import sell.common.security.context.UserContextHolder;
import sell.user.persistence.mapper.ExtraInfoMapper;
import sell.user.persistence.mapper.UserMapper;
import sell.user.persistence.mapper.UserRoleMapper;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.po.UserPO;
import sell.user.persistence.po.UserRolePO;
import sell.user.persistence.repository.AdminRepository;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Repository
@RequiredArgsConstructor
public class AdminRepositoryImpl implements AdminRepository {

    private final UserMapper userMapper;
    private final UserRoleMapper userRoleMapper;
    private final ExtraInfoMapper extraInfoMapper;
    @Reference(check = false)
    private AccountRpcFacade accountRpcFacade;

    @Override
    public Boolean changeIsLock(Boolean isLock, Long id) {

        if (UserContextHolder.getUserId().equals(id)){
            throw new ServiceException("当前登陆用户状态无法修改!");
        }

        return new LambdaUpdateChainWrapper<>(userMapper).eq(UserPO::getId, id).set(UserPO::getIsLock, isLock).update();

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long addAdmin(UserPO user) {
        long id = IdWorker.getId();

        Assert.isNull(userMapper.selectOne(Wrappers.<UserPO>lambdaQuery().eq(UserPO::getMobile,user.getMobile())),"手机号已存在！");

        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.setRole(UserTypeEnum.ADMIN);
        user.setIsLock(Boolean.FALSE);
        user.setId(id);

        UserRolePO userRole = UserRolePO.builder()
                .roleId(Long.valueOf(UserTypeEnum.ADMIN.getValue()))
                .uid(id)
                .build();

        userMapper.insert(user);
        userRoleMapper.insert(userRole);
        accountRpcFacade.initAccount(id);
        return id;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteAdmin(Long id) {
        if (UserContextHolder.getUserId().equals(id)) {
            throw new ServiceException("无法删除本人的账号!");
        }
        List<UserPO> allAdmin = userMapper.selectList(Wrappers.<UserPO>lambdaQuery().eq(UserPO::getRole, UserTypeEnum.ADMIN));
        if (allAdmin.size()<=1){
            throw new ServiceException("请至少保留一个管理员用户");
        }
        userMapper.deleteById(id);
        userRoleMapper.delete(Wrappers.<UserRolePO>lambdaQuery().eq(UserRolePO::getUid,id));
        extraInfoMapper.delete(Wrappers.<ExtraInfoPO>lambdaQuery().eq(ExtraInfoPO::getUid,id));
        accountRpcFacade.deleteAccount(id);

    }

    @Override
    public void updateAdminBaseInfo(Long id, String mobile, String nickName, SexTypeEnum sex) {

        UserPO po = userMapper.selectById(id);

        if (!po.getMobile().equals(mobile)){
            Assert.isNull(userMapper.selectOne(Wrappers.<UserPO>lambdaQuery().eq(UserPO::getMobile,mobile)),"手机号已存在！");
            po.setMobile(mobile);
        }

        po.setNickName(nickName);
        po.setSex(sex);


        userMapper.updateById(po);
    }
}
