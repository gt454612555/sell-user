package sell.user.persistence.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("消息列表分页查询--返回参数")
public class MessagePageResDTO {

    @ApiModelProperty(value = "消息id")
    private Long id;

    @ApiModelProperty(value = "消息内容")
    private String content;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

}
