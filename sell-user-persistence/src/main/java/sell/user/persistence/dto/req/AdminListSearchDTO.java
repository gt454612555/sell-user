package sell.user.persistence.dto.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import sell.user.persistence.dto.res.AdminListResDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "搜索管理员用户 请求入参")
public class AdminListSearchDTO extends Page<AdminListResDTO> {

    @ApiModelProperty(value = "关键字--手机号，昵称")
    private String keyWord;

}
