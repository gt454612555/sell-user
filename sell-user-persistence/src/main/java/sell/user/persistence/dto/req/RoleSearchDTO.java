package sell.user.persistence.dto.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import sell.user.persistence.po.RolePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "角色分页搜索入参")
public class RoleSearchDTO extends Page<RolePO> {

    @ApiModelProperty(value = "角色名称")
    private String name;

}
