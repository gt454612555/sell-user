package sell.user.persistence.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sell.common.enums.user.SexTypeEnum;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("管理员列表分页查询--返回参数")
public class AdminListResDTO {

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "性别")
    private SexTypeEnum sex;

    @ApiModelProperty(value = "添加时间")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "是否锁定")
    private Boolean isLock;

}
