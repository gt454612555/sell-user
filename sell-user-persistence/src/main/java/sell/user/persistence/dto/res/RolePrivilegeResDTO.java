package sell.user.persistence.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "角色权限返回数据")
public class RolePrivilegeResDTO {

    @ApiModelProperty(value = "角色id--1、顾客 2、商家 3、管理员")
    private Integer roleId;

    @ApiModelProperty(value = "权限id")
    private Long id;

    @ApiModelProperty(value = "权限模块分类名称")
    private String name;

    @ApiModelProperty(value = "权限描述")
    private String description;

}
