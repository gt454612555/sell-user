package sell.user.persistence.dto.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import sell.user.persistence.po.UserPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "用户搜索入参")
public class UserSearchDTO extends Page<UserPO> {

    @ApiModelProperty(value = "关键字--手机号/昵称")
    private String keyWord;

}
