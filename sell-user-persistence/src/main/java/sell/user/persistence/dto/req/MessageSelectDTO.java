package sell.user.persistence.dto.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import sell.common.enums.message.MessageTypeEnum;
import sell.user.persistence.dto.res.MessagePageResDTO;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "消息选择分页查询 请求入参")
public class MessageSelectDTO extends Page<MessagePageResDTO> {
    @ApiModelProperty(value = "消息类型")
    @NotNull
    private MessageTypeEnum type;
}
