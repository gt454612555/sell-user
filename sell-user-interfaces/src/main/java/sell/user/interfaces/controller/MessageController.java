package sell.user.interfaces.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sell.common.api.result.PageResult;
import sell.common.api.result.Result;
import sell.user.interfaces.assembler.MessageAssembler;
import sell.user.interfaces.vo.req.SendSysMsgVO;
import sell.user.persistence.dto.req.MessageSelectDTO;
import sell.user.persistence.dto.res.MessagePageResDTO;
import sell.user.services.service.MessageService;
import sell.user.services.service.query.MessageQueryService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/3/27
 */
@Api(tags = "消息相关接口")
@RestController
@RequiredArgsConstructor
@RequestMapping("/message")
public class MessageController {

    private final MessageService messageService;
    private final MessageQueryService messageQueryService;
    private final MessageAssembler messageAssembler;

    @ApiOperation("发送系统公告")
    @RequestMapping(value = "/sendSysMsg",method = RequestMethod.POST)
    public Result<Long> sendSysMsg(@RequestBody @Validated SendSysMsgVO vo){
        Long msgId = messageService.sendSysMsg(vo.getContent(), vo.getObj());
        return Result.succeed(msgId);
    }

    @ApiOperation("分页查询消息列表")
    @RequestMapping(value = "/getMsgPage",method = RequestMethod.POST)
    public Result<PageResult<MessagePageResDTO>> getMsgPage(@RequestBody @Validated MessageSelectDTO select){
        Page<MessagePageResDTO> msgPage = messageQueryService.getMsgPage(select);
        PageResult<MessagePageResDTO> resPage = messageAssembler.msgPageToPageRes(msgPage);
        return Result.succeed(resPage);
    }

    @ApiOperation("获取未读消息数量")
    @RequestMapping(value = "/getUnReadMsgNum",method = RequestMethod.GET)
    public Result<Integer> getUnReadNum(){
        Integer unReadMsgNum = messageQueryService.getUnReadMsgNum();
        return Result.succeed(unReadMsgNum);
    }

    @ApiOperation("已读一条消息")
    @RequestMapping(value = "/readMsg",method = RequestMethod.GET)
    public Result<?> readMsg(@ApiParam(name = "id",value = "消息id") @RequestParam("id")Long id){
        messageService.readMsg(id);
        return Result.succeed();
    }

    @ApiOperation("全部消息已读")
    @RequestMapping(value = "/readAll",method = RequestMethod.GET)
    public Result<?> readAll(){
        messageService.readAll();
        return Result.succeed();
    }

    @ApiOperation("获取最近的消息列表")
    @RequestMapping(value = "/getAppMsgPage",method = RequestMethod.POST)
    public Result<PageResult<MessagePageResDTO>> getAppPage(Page<MessagePageResDTO> query){
        Page<MessagePageResDTO> myAppMsg = messageQueryService.getMyAppMsg(query);
        PageResult<MessagePageResDTO> resPage = messageAssembler.msgPageToPageRes(myAppMsg);
        return Result.succeed(resPage);
    }
}
