package sell.user.interfaces.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sell.common.api.anno.AvoidRepeatableCommit;
import sell.common.api.dto.CustomerAddrDTO;
import sell.common.api.result.PageResult;
import sell.common.api.result.Result;
import sell.common.security.context.UserContextHolder;
import sell.user.interfaces.assembler.UserAssembler;
import sell.user.interfaces.vo.req.*;
import sell.user.interfaces.vo.res.CustomerAddrResVO;
import sell.user.interfaces.vo.res.CustomerInfoAddrResVO;
import sell.user.interfaces.vo.res.SellerInfoResVO;
import sell.user.persistence.dto.req.UserSearchDTO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.po.UserPO;
import sell.user.services.dto.res.CustomerAddrResDTO;
import sell.user.services.service.UserService;
import sell.user.services.dto.res.CustomerInfoAddrResDTO;
import sell.user.services.service.query.UserQueryService;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Api(tags = "用户相关接口")
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserAssembler userAssembler;
    private final UserQueryService userQueryService;


    @AvoidRepeatableCommit
    @ApiOperation(value = "注册新用户")
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public Result<Map<String,Object>> register(@RequestBody @Validated UserRegisterVO vo){
        Map<String, Object> resMap = userService.userRegister(userAssembler.userVoToDto(vo));
        return Result.succeed(resMap);
    }

    @AvoidRepeatableCommit
    @ApiOperation(value = "修改密码--通过验证旧密码")
    @RequestMapping(value = "/changePassword",method = RequestMethod.POST)
    public Result<Map<String,Object>> changePassword(@RequestBody @Validated ChangePasswordVO vo){
        Map<String, Object> resMap = userService.changePasswordByOld(vo.getOldPassword(), vo.getNewPassword());
        return Result.succeed(resMap);
    }

    @AvoidRepeatableCommit
    @ApiOperation(value = "设置用户信息")
    @RequestMapping(value = "/setExtraInfo",method = RequestMethod.POST)
    public Result<Boolean> setCustomerInfo(@RequestBody @Validated ExtraInfoVO vo){
        ExtraInfoPO po = userAssembler.extraInfoVoToPo(vo);
        Boolean res = userService.setExtraInfo(po);
        return Result.succeed(res);
    }

    /**
     * 新增一个收货地址：
     *  先查询有无默认收货地址，如果没有就设置当前为默认地址
     * @return 新增地址的id
     */
    @ApiOperation(value = "新增一个收货地址,并返回收货地址id")
    @RequestMapping(value = "/addAddr",method = RequestMethod.POST)
    public Result<Long> addAddr(@RequestBody @Validated CustomerAddrVO vo){
        Long id = userService.addAddr(userAssembler.customerAddrVoToPo(vo));
        return Result.succeed(id);
    }

    @ApiOperation(value = "删除一个收货地址")
    @RequestMapping(value = "/deleteAddr",method = RequestMethod.GET)
    public Result<Boolean> deleteAddr(@ApiParam(name = "addrId",value = "收货地址id") @RequestParam("addrId") @NotNull Long addrId){
        Boolean res = userService.deleteAddr(addrId);
        return Result.succeed(res);
    }

    @ApiOperation(value = "查询顾客用户的其他信息")
    @RequestMapping(value = "/showCustomerInfo",method = RequestMethod.GET)
    public Result<ExtraInfoPO> getInfo(){
        ExtraInfoPO extraInfo = userService.getExtraInfo();
        return Result.succeed(extraInfo);
    }

    @ApiOperation(value = "查询商家用户的信息")
    @RequestMapping(value = "/showSellerInfo",method = RequestMethod.GET)
    public Result<SellerInfoResVO> getSellerInfo(){
        ExtraInfoPO extraInfo = userService.getExtraInfo();
        return Result.succeed(userAssembler.extraInfoPoToSellerVo(extraInfo));
    }


    @ApiOperation(value = "分页查询所有顾客用户信息")
    @RequestMapping(value = "/getCustomerPage",method = RequestMethod.POST)
    public Result<PageResult<UserPO>> getCustomerPage(@RequestBody UserSearchDTO search){
        Page<UserPO> page = userQueryService.getCustomerPage(search);
        return Result.succeed(userAssembler.userPageToPageRes(page));
    }

    @ApiOperation(value = "分页查询所有商家用户信息")
    @RequestMapping(value = "/getSellerPage",method = RequestMethod.POST)
    public Result<PageResult<UserPO>> getSellerPage(@RequestBody UserSearchDTO search){
        Page<UserPO> page = userQueryService.getSellerPage(search);
        return Result.succeed(userAssembler.userPageToPageRes(page));
    }

    @ApiOperation(value = "重置密码为123456")
    @RequestMapping(value = "/resetPassword",method = RequestMethod.GET)
    public Result<?> resetPassword(@ApiParam(name = "id",value = "用户id") @RequestParam("id") Long id){
        userService.resetPassword(id);
        return Result.succeed();
    }

    @ApiOperation(value = "删除一个用户")
    @RequestMapping(value = "/deleteUser",method = RequestMethod.GET)
    public Result<?> deleteUser(@ApiParam(name = "id",value = "用户id") @RequestParam("id") Long id){
        userService.deleteUser(id);
        return Result.succeed();
    }


    @ApiOperation(value = "查询购物车数量")
    @RequestMapping(value = "/getCartNum",method = RequestMethod.GET)
    public Result<Integer> getCartNum(){
        Integer cartNum = userService.getCartNum();
        return Result.succeed(cartNum);
    }

    @ApiOperation(value = "修改购物车的商品数量")
    @RequestMapping(value = "/changeCartNum",method = RequestMethod.POST)
    public Result<?> changeCartNum(@RequestBody @Validated CartNumChangeVO vo){
        userService.changeCartBuyNum(vo.getId(),vo.getNum());
        return Result.succeed();
    }

    @ApiOperation(value = "获取所有收货地址列表")
    @RequestMapping(value = "/getAddrList",method = RequestMethod.GET)
    public Result<List<CustomerAddrResVO>> getAddrList(){
        List<CustomerAddrResDTO> addrList = userService.getAddrList();
        List<CustomerAddrResVO> vos = userAssembler.addrDtoToVo(addrList);
        return Result.succeed(vos);
    }

    @ApiOperation(value = "查询收货地址详情")
    @RequestMapping(value = "/getAddrInfo",method = RequestMethod.GET)
    public Result<CustomerAddrDTO> getAddrInfo(@RequestParam("addrId")Long addrId){
        CustomerAddrDTO res = userService.getAndCheckAddrInfo(UserContextHolder.getUserId(), addrId);
        return Result.succeed(res);
    }

    @ApiOperation(value = "设置默认收货地址")
    @RequestMapping(value = "/setDefaultAddr",method = RequestMethod.GET)
    public Result<?> setDefaultAddr(@RequestParam("addrId")Long addrId){
        userService.setDefaultAddr(addrId);
        return Result.succeed();
    }

    @ApiOperation(value = "查询默认收货地址")
    @RequestMapping(value = "/getDefaultAddr",method = RequestMethod.GET)
    public Result<CustomerAddrDTO> getCustomerAddr(){
        CustomerAddrDTO defaultAddr = userService.getDefaultAddr(UserContextHolder.getUserId());
        return Result.succeed(defaultAddr);
    }

    @ApiOperation("查询账户余额")
    @RequestMapping(value = "/showBalance",method = RequestMethod.GET)
    public Result<BigDecimal> showBalance(){
        BigDecimal balance = userService.getBalance();
        return Result.succeed(balance);
    }

    @ApiOperation("更换头像")
    @RequestMapping(value = "/changeAvatar",method = RequestMethod.GET)
    public Result<?> changeAvatar(@RequestParam("avatar")Long avatar){
        userService.changeAvatar(avatar);
        return Result.succeed();
    }

}
