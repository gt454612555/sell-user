package sell.user.interfaces.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sell.common.api.result.PageResult;
import sell.common.api.result.Result;
import sell.user.interfaces.assembler.AdminAssembler;
import sell.user.interfaces.vo.req.AdminAddVO;
import sell.user.interfaces.vo.req.AdminBaseInfoUpdateVO;
import sell.user.interfaces.vo.req.AdminIsLockVO;
import sell.user.persistence.dto.req.AdminListSearchDTO;
import sell.user.persistence.dto.res.AdminListResDTO;
import sell.user.services.dto.UserDTO;
import sell.user.services.service.AdminService;
import sell.user.services.service.query.AdminQueryService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Api(tags = "管理员用户相关接口")
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {


    private final AdminQueryService adminQueryService;
    private final AdminService adminService;
    private final AdminAssembler adminAssembler;


    @ApiOperation("分页查询所有的管理员")
    @RequestMapping(value = "/getAdminPage",method = RequestMethod.POST)
    public Result<PageResult<AdminListResDTO>> getAdminPage(@RequestBody AdminListSearchDTO search){
        Page<AdminListResDTO> page = adminQueryService.getAdminList(search);
        return Result.succeed(adminAssembler.adminListResDTOPageResult(page));
    }

    @ApiOperation("启用/禁用账户")
    @RequestMapping(value = "/changeIsLock",method = RequestMethod.POST)
    public Result<?> changeIsLock(@RequestBody @Validated AdminIsLockVO vo){
        adminService.changeIsLock(vo.getIsLock(),vo.getId());
        return Result.succeed();
    }

    @ApiOperation("添加管理员用户")
    @RequestMapping(value = "/addAdmin",method = RequestMethod.POST)
    public Result<Long> addAdmin(@RequestBody @Validated AdminAddVO vo){
        UserDTO userDTO = adminAssembler.adminAddVoToUser(vo);
        Long uid = adminService.addAdmin(userDTO);
        return Result.succeed(uid);
    }

    @ApiOperation("删除管理员账户")
    @RequestMapping(value = "/deleteAdmin",method = RequestMethod.GET)
    public Result<?> deleteAdmin(@ApiParam(name = "id",value = "管理员用户id") @RequestParam("id")Long id){
        adminService.deleteAdmin(id);
        return Result.succeed();
    }

    @ApiOperation("修改管理员用户信息")
    @RequestMapping(value = "/updateBaseInfo",method = RequestMethod.POST)
    public Result<?> updateAdminBaseInfo(@RequestBody @Validated AdminBaseInfoUpdateVO vo){
        adminService.changeAdminBaseInfo(adminAssembler.adminInfoVoToDto(vo));
        return Result.succeed();
    }

}
