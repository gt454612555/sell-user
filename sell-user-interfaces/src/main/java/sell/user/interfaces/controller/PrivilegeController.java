package sell.user.interfaces.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sell.common.api.result.PageResult;
import sell.common.api.result.Result;
import sell.common.dal.po.AllPrivilegeResPO;
import sell.common.dal.po.PrivilegePO;
import sell.common.dal.vo.PrivilegeSearchVO;
import sell.user.interfaces.assembler.PrivilegeAssembler;
import sell.user.persistence.dto.req.RoleSearchDTO;
import sell.user.persistence.dto.res.RolePrivilegeResDTO;
import sell.user.persistence.po.RolePO;
import sell.user.services.dto.req.PrivilegeChangeInfoDTO;
import sell.user.services.dto.req.SetPrivilegeDTO;
import sell.user.services.dto.res.PrivilegeClassNameResDTO;
import sell.user.services.service.PrivilegeService;
import sell.user.services.service.query.PrivilegeQueryService;
import sell.user.services.service.query.RoleQueryService;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Api(tags = "权限相关接口")
@RestController
@RequiredArgsConstructor
@RequestMapping("/privilege")
public class PrivilegeController {

    private final PrivilegeQueryService privilegeQueryService;
    private final PrivilegeService privilegeService;
    private final PrivilegeAssembler privilegeAssembler;
    private final RoleQueryService roleQueryService;

    @ApiOperation("获取请求url列表--分页查询")
    @RequestMapping(value = "/getPrivilegePage",method = RequestMethod.POST)
    public Result<PageResult<PrivilegePO>> getPrivilegePage(@RequestBody PrivilegeSearchVO vo){

        Page<PrivilegePO> page = privilegeQueryService.getPrivilegePage(vo);

        return Result.succeed(privilegeAssembler.privilegePageToPageRes(page));

    }

    @ApiOperation("获取所有权限分类列表")
    @RequestMapping(value = "/getAllPrivilegeClass",method = RequestMethod.GET)
    public Result<List<PrivilegeClassNameResDTO>> getAllPrivilegeClass(){
        List<PrivilegeClassNameResDTO> res = privilegeQueryService.getAllPrivilegeClass();
        return Result.succeed(res);
    }

    @ApiOperation("修改权限模块名和描述")
    @RequestMapping(value = "/changePrivilegeInfo",method = RequestMethod.POST)
    public Result<?> changeInfo(@RequestBody @Validated PrivilegeChangeInfoDTO dto){
        privilegeService.changePrivilegeInfo(dto);
        return Result.succeed();
    }

    @ApiOperation("分页查询所有角色列表")
    @RequestMapping(value = "/getRolePage",method = RequestMethod.POST)
    public Result<PageResult<RolePO>> getRolePage(@RequestBody RoleSearchDTO search){

        Page<RolePO> page = roleQueryService.getRolePage(search);

        return Result.succeed(privilegeAssembler.roleToPageRes(page));

    }

    @ApiOperation("查询所有权限列表")
    @RequestMapping(value = "/getAllPrivilegeList",method = RequestMethod.GET)
    public Result<List<AllPrivilegeResPO>> getAllPrivilege(){
        List<AllPrivilegeResPO> resList = privilegeQueryService.getAllPrivilege();
        return Result.succeed(resList);
    }

    @ApiOperation("根据角色id查询已配置的权限")
    @RequestMapping(value = "/getPrivilegeByRole",method = RequestMethod.GET)
    public Result<List<RolePrivilegeResDTO>> getPrivilegeByRole(@ApiParam(name = "roleId",value = "角色ID") @RequestParam("roleId") Integer roleId){
        List<RolePrivilegeResDTO> resList = privilegeQueryService.getPrivilegeByRole(roleId);
        return Result.succeed(resList);
    }

    @ApiOperation("为角色分配权限")
    @RequestMapping(value = "/setPrivilege",method = RequestMethod.POST)
    public Result<?> setPrivilege(@RequestBody @Validated SetPrivilegeDTO dto){
        privilegeService.setPrivilege(dto);
        return Result.succeed();
    }


}
