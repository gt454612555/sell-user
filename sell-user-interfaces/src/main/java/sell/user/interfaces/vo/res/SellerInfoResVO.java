package sell.user.interfaces.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/3/27
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户信息页面查询 返回参数")
public class SellerInfoResVO {

    @ApiModelProperty(value = "当前用户id")
    private Long uid;

    @ApiModelProperty(value = "真实姓名",required = true)
    private String realName;

    @ApiModelProperty(value = "主体名称",required = true)
    private String entityName;

    @ApiModelProperty(value = "邮箱",required = true)
    private String email;

    @ApiModelProperty(value = "年龄",required = true)
    private Integer age;

    @ApiModelProperty(value = "支付宝账户--提现专用",required = true)
    private String aliPay;

}
