package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sell.common.enums.message.BroadcastObjEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "发送系统公告入参")
public class SendSysMsgVO {

    @ApiModelProperty(value = "消息内容")
    @NotBlank
    private String content;

    @ApiModelProperty(value = "消息广播对象")
    @NotNull
    private BroadcastObjEnum obj;

}
