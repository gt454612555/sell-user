package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "新增收货地址入参VO")
public class CustomerAddrVO {

    @ApiModelProperty(value = "省编码",required = true)
    @NotBlank
    private String provinceCode;

    @ApiModelProperty(value = "市编码",required = true)
    @NotBlank
    private String cityCode;

    @ApiModelProperty(value = "区编码")
    private String areaCode;

    @ApiModelProperty(value = "详细地址",required = true)
    @NotBlank
    private String address;

    @ApiModelProperty(value = "收件人手机号",required = true)
    @NotBlank
    @Pattern(regexp = "^1[3456789]\\d{9}$",message = "手机号格式错误")
    private String userMobile;

    @ApiModelProperty(value = "收件人姓名",required = true)
    @NotBlank
    private String userName;

}
