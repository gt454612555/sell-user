package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import sell.common.enums.user.SexTypeEnum;
import sell.common.enums.user.UserTypeEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "注册用户前端入参对象")
public class UserRegisterVO {

    @ApiModelProperty(value = "用户手机号-用户名",required = true)
    @Pattern(regexp = "^1[3456789]\\d{9}$",message = "手机号格式错误")
    @NotBlank
    private String mobile;

    @ApiModelProperty(value = "密码",required = true)
    @Length(min = 6,max = 20,message = "密码长度必须为6-20位")
    @NotBlank
    private String password;

    @ApiModelProperty(value = "性别：1-男性 2-女性",required = true)
    @NotNull
    private SexTypeEnum sex;

    @ApiModelProperty(value = "昵称",required = true)
    @NotBlank
    @Length(min = 3,max = 8,message = "用户昵称为3-8位")
    private String nickName;

    @ApiModelProperty(value = "注册类型：1-顾客 2-商家",required = true)
    @NotNull
    private UserTypeEnum role;
}
