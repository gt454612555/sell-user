package sell.user.interfaces.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户收货地址 返回参数")
public class CustomerAddrResVO {

    @ApiModelProperty(value = "收货地址id",required = true)
    private Long id;

    @ApiModelProperty(value = "省编码",required = true)
    private String provinceCode;

    @ApiModelProperty(value = "省名称",required = true)
    private String provinceName;

    @ApiModelProperty(value = "市编码",required = true)
    private String cityCode;

    @ApiModelProperty(value = "市名称",required = true)
    private String cityName;

    @ApiModelProperty(value = "区编码")
    private String areaCode;

    @ApiModelProperty(value = "区名称")
    private String areaName;

    @ApiModelProperty(value = "详细地址",required = true)
    private String address;

    @ApiModelProperty(value = "收件人手机号",required = true)
    private String userMobile;

    @ApiModelProperty(value = "收件人姓名",required = true)
    private String userName;

    @ApiModelProperty(value = "是否为默认收货地址",required = true)
    private Boolean isDefault;

}
