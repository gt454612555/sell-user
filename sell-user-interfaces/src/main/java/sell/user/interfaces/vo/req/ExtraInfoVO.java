package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "设置其他信息入参VO")
public class ExtraInfoVO {

    @ApiModelProperty(value = "真实姓名",required = true)
    @NotBlank
    private String realName;

    @ApiModelProperty(value = "实体名称--商家填写可选项")
    private String entityName;

    @ApiModelProperty(value = "邮箱",required = true)
    @NotBlank
    @Pattern(regexp = "^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$")
    private String email;

    @ApiModelProperty(value = "年龄",required = true)
    @NotNull
    @Min(value = 1L)
    private Integer age;

    @ApiModelProperty(value = "支付宝账户--退款专用",required = true)
    @NotBlank
    private String aliPay;
}
