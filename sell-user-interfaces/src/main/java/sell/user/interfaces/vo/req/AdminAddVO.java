package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import sell.common.enums.user.SexTypeEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "管理员账户注册---前端入参对象")
public class AdminAddVO {

    @ApiModelProperty(value = "昵称")
    @NotBlank
    private String nickName;

    @ApiModelProperty(value = "手机号")
    @NotBlank
    @Pattern(regexp = "^1[3456789]\\d{9}$",message = "手机号格式错误")
    private String mobile;

    @ApiModelProperty(value = "密码")
    @Length(min = 6,max = 20,message = "密码长度为6-20位")
    @NotBlank
    private String password;

    @ApiModelProperty(value = "性别")
    @NotNull
    private SexTypeEnum sex;

}
