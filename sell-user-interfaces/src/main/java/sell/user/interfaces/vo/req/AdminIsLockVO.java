package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "管理员账户是否禁用---前端入参对象")
public class AdminIsLockVO {

    @ApiModelProperty(value = "是否锁定")
    @NotNull
    private Boolean isLock;

    @ApiModelProperty(value = "账号id")
    @NotNull
    private Long id;

}
