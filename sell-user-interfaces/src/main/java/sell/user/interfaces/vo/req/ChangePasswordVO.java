package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "修改密码前端入参对象")
public class ChangePasswordVO {

    @ApiModelProperty(value = "旧密码",required = true)
    @Length(min = 6,max = 20,message = "密码长度必须为6-20位")
    @NotBlank
    private String oldPassword;

    @ApiModelProperty(value = "新密码",required = true)
    @Length(min = 6,max = 20,message = "密码长度必须为6-20位")
    @NotBlank
    private String newPassword;


}
