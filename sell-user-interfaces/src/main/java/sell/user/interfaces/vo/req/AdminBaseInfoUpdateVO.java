package sell.user.interfaces.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sell.common.enums.user.SexTypeEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "管理员账户修改基本信息---前端入参对象")
public class AdminBaseInfoUpdateVO {

    @ApiModelProperty(value = "用户id")
    @NotNull
    private Long id;

    @ApiModelProperty(value = "用户手机号")
    @Pattern(regexp = "^1[3456789]\\d{9}$",message = "手机号格式错误")
    @NotBlank
    private String mobile;

    @ApiModelProperty(value = "昵称")
    @NotBlank
    private String nickName;

    @ApiModelProperty(value = "性别")
    @NotNull
    private SexTypeEnum sex;
}
