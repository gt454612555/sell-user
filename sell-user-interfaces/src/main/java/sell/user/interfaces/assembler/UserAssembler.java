package sell.user.interfaces.assembler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.result.PageResult;
import sell.user.interfaces.vo.req.CustomerAddrVO;
import sell.user.interfaces.vo.req.ExtraInfoVO;
import sell.user.interfaces.vo.res.CustomerAddrResVO;
import sell.user.interfaces.vo.res.CustomerInfoAddrResVO;
import sell.user.interfaces.vo.res.SellerInfoResVO;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.po.UserPO;
import sell.user.services.dto.UserDTO;
import sell.user.interfaces.vo.req.UserRegisterVO;
import sell.user.services.dto.res.CustomerAddrResDTO;
import sell.user.services.dto.res.CustomerInfoAddrResDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserAssembler {
    /**
     * 注册前端入参 TO 业务DTO对象
     * @param register
     * @return
     */
    UserDTO userVoToDto(UserRegisterVO register);

    /**
     * 用户其他信息vo to po
     * @param vo
     * @return
     */
    ExtraInfoPO extraInfoVoToPo(ExtraInfoVO vo);

    /**
     * 用户其他信息po 2 商家 vo
     * @param po
     * @return
     */
    SellerInfoResVO extraInfoPoToSellerVo(ExtraInfoPO po);

    /**
     * 收货地址vo to po
     * @param vo
     * @return
     */
    CustomerAddrPO customerAddrVoToPo(CustomerAddrVO vo);

    CustomerInfoAddrResVO infoAddrDtoToVo(CustomerInfoAddrResDTO dto);

    List<CustomerAddrResVO> addrDtoToVo(List<CustomerAddrResDTO> dtoList);

    PageResult<UserPO> userPageToPageRes(Page<UserPO> page);

}
