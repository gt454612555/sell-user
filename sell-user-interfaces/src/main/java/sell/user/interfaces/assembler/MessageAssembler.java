package sell.user.interfaces.assembler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.result.PageResult;
import sell.user.persistence.dto.res.MessagePageResDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/24
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface MessageAssembler {


    PageResult<MessagePageResDTO> msgPageToPageRes(Page<MessagePageResDTO> page);

}
