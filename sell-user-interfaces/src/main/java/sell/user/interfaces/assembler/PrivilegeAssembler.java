package sell.user.interfaces.assembler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.result.PageResult;
import sell.common.dal.po.PrivilegePO;
import sell.user.persistence.po.RolePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface PrivilegeAssembler {

    PageResult<PrivilegePO> privilegePageToPageRes(Page<PrivilegePO> page);

    PageResult<RolePO> roleToPageRes(Page<RolePO> page);

}
