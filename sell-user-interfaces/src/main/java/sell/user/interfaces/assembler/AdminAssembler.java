package sell.user.interfaces.assembler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.result.PageResult;
import sell.user.interfaces.vo.req.AdminAddVO;
import sell.user.interfaces.vo.req.AdminBaseInfoUpdateVO;
import sell.user.persistence.dto.res.AdminListResDTO;
import sell.user.services.dto.UserDTO;
import sell.user.services.dto.req.AdminBaseInfoDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface AdminAssembler {

    PageResult<AdminListResDTO> adminListResDTOPageResult(Page<AdminListResDTO> page);

    UserDTO adminAddVoToUser(AdminAddVO vo);

    AdminBaseInfoDTO adminInfoVoToDto(AdminBaseInfoUpdateVO vo);

}
