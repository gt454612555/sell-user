package sell.user.rpc.factory;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.dto.ExtraInfoDTO;
import sell.user.persistence.po.ExtraInfoPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/14
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserRpcFactory {

    ExtraInfoDTO extraInfoPoToDto(ExtraInfoPO po);

}
