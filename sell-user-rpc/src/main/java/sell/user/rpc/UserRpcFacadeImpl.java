package sell.user.rpc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import sell.common.api.dto.CustomerAddrDTO;
import sell.common.api.dto.CustomerCartResDTO;
import sell.common.api.dto.ExtraInfoDTO;
import sell.common.api.rpc.UserRpcFacade;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.CustomerCartPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.repository.UserRepository;
import sell.user.rpc.factory.UserRpcFactory;
import sell.user.services.service.UserService;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/10
 */
@RequiredArgsConstructor
@Slf4j
@Service(interfaceClass = UserRpcFacade.class)
public class UserRpcFacadeImpl implements UserRpcFacade {

    private final UserRepository userRepository;
    private final UserService userService;
    private final UserRpcFactory userRpcFactory;

    @Override
    public String getMobileByUid(Long uid) {
        return userRepository.getMobileByUid(uid);
    }

    @Override
    public CustomerAddrDTO getCustomerAddr(Long uid, Long addrId) {
        return userService.getAndCheckAddrInfo(uid, addrId);
    }

    @Override
    public CustomerAddrDTO getDefaultAddr(Long uid) {
        return userService.getDefaultAddr(uid);
    }

    @Override
    public Long addCart(Long uid, Long goodsId, Integer buyNum) {
        return userRepository.addGoodsIntoCart(uid, goodsId, buyNum);
    }

    @Override
    public CustomerCartResDTO getCartInfo(Long cartId) {
        CustomerCartPO cartInfoPo = userRepository.getCartInfoById(cartId);
        if (cartInfoPo==null){
            return null;
        }
        return CustomerCartResDTO.builder()
                .id(cartId)
                .buyNum(cartInfoPo.getBuyNum())
                .goodsId(cartInfoPo.getGoodsId())
                .uid(cartInfoPo.getUid())
                .build();
    }

    @Override
    public ExtraInfoDTO getExtraInfo(Long uid) {
        ExtraInfoPO extraInfo = userRepository.getExtraInfo(uid);
        return userRpcFactory.extraInfoPoToDto(extraInfo);
    }

    @Override
    public void deleteCart(Long cartId) {
        userRepository.deleteCart(cartId);
    }

    @Override
    public void deleteCart(List<Long> cartIds) {
        userRepository.deleteCart(cartIds);
    }

    @Override
    public void deleteCartByUserId(Long uid) {
        userRepository.deleteCartByUid(uid);
    }


}
