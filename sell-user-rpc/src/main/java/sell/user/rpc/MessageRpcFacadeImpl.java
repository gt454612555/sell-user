package sell.user.rpc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import sell.common.api.dto.SendMsgDTO;
import sell.common.api.rpc.MessageRpcFacade;
import sell.user.persistence.repository.MessageRepository;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@RequiredArgsConstructor
@Service(interfaceClass = MessageRpcFacade.class)
@Slf4j
public class MessageRpcFacadeImpl implements MessageRpcFacade {

    private final MessageRepository messageRepository;

    @Override
    public Long sendMsg(SendMsgDTO dto) {
        try {
            return messageRepository.sendMsg(dto);
        } catch (Exception e) {
            log.info("消息发送对象=>{},发送失败，原因=>{}",dto.toString(),e.getMessage());
        }
        return null;
    }
}
