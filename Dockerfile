FROM java:8

MAINTAINER g.t<454612555@qq.com>

ENV TZ=Asia/Shanghai

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime; \
    echo $TZ > /etc/timezone;

VOLUME ["/sell"]

COPY /sell-user-bootstrap/build/libs/*.jar /sell/app.jar

CMD ["--server.port=80"]

EXPOSE 80

ENTRYPOINT ["sh","-c","java -Xmx100m -Xms100m -Xmn24m -XX:NewRatio=2 -XX:SurvivorRatio=8 -Dfile.encoding=utf-8 -Dspring.profiles.active=prod -Duser.timezone=GMT+8 -jar /sell/app.jar"]

