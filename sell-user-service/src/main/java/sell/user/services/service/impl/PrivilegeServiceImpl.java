package sell.user.services.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sell.common.dal.mapper.PrivilegeMapper;
import sell.common.dal.po.PrivilegePO;
import sell.user.persistence.mapper.RolePrivilegeMapper;
import sell.user.persistence.po.RolePrivilegePO;
import sell.user.services.dto.req.PrivilegeChangeInfoDTO;
import sell.user.services.dto.req.SetPrivilegeDTO;
import sell.user.services.service.PrivilegeService;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Service
@RequiredArgsConstructor
public class PrivilegeServiceImpl implements PrivilegeService {

    private final PrivilegeMapper privilegeMapper;
    private final RolePrivilegeMapper rolePrivilegeMapper;

    @Override
    public void changePrivilegeInfo(PrivilegeChangeInfoDTO dto) {

        new LambdaUpdateChainWrapper<>(privilegeMapper)
                .eq(PrivilegePO::getId,dto.getId())
                .set(PrivilegePO::getName,dto.getName())
                .set(PrivilegePO::getDescription,dto.getDescription())
                .update();

    }

    /**
     * 判断两个Long泛型list是否含有的元素一摸一样
     * @param list1
     * @param list2
     * @return
     */
    private boolean isSame(List<Long> list1,List<Long> list2){

        if (list1 == null && list2 == null){
            return true;
        }
        if (list1 == list2){
            return true;
        }
        if (list1 == null || list2 == null){
            return false;
        }
        if (list1.size()!=list2.size()){
            return false;
        }
        return list1.containsAll(list2) && list2.containsAll(list1);
    }

    /**
     * 取两集合的交集，并返回
     * @param s1
     * @param s2
     * @return
     */
    private Set<Long> getIntersection(Set<Long> s1,Set<Long> s2){
        Set<Long> res = new HashSet<>();
        for (Long item : s1) {
            if (s2.contains(item)) {
                res.add(item);
            }
        }
        return res;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setPrivilege(SetPrivilegeDTO dto) {
        @NotNull List<Long> unSaved = dto.getPrivilegeIds();
        @NotNull Integer roleId = dto.getRoleId();
        //数据库中已保存的权限
        List<Long> savedPrivileges = rolePrivilegeMapper.getSavedPrivilegeIdsByRole(roleId);
        if (CollectionUtils.isEmpty(unSaved)){
            //去除当前角色所有的权限
            rolePrivilegeMapper.delete(Wrappers.<RolePrivilegePO>lambdaQuery().eq(RolePrivilegePO::getRoleId,roleId));
            return;
        }
        if (CollectionUtils.isEmpty(savedPrivileges)){
            //数据库中没有设置权限，全部添加
            if (CollectionUtils.isNotEmpty(unSaved)){
                List<RolePrivilegePO> pos = unSaved.stream().map(e -> RolePrivilegePO.builder().roleId(Long.valueOf(roleId)).status(Boolean.TRUE).privilegeId(e).build()).collect(Collectors.toList());
                pos.forEach(rolePrivilegeMapper::insert);
            }
            return;
        }
        //解决没有任何改动点保存的Bug
        if (isSame(unSaved,savedPrivileges)){
            return;
        }
        /*--------排除两个集合都是空的情况------------*/
        Set<Long> unSaveClone = new HashSet<>(unSaved);
        Set<Long> savedClone = new HashSet<>(savedPrivileges);
        Set<Long> intersection = getIntersection(unSaveClone, savedClone);
        if (CollectionUtils.isNotEmpty(intersection)){
            //两个集合相交
            //删除
            savedClone.removeAll(intersection);
            if (CollectionUtils.isNotEmpty(savedClone)){
                //删除集合中的元素对应数据库
                savedClone.forEach(e->rolePrivilegeMapper.delete(Wrappers.<RolePrivilegePO>lambdaQuery().eq(RolePrivilegePO::getPrivilegeId,e).eq(RolePrivilegePO::getRoleId,roleId)));
            }

            //添加
            unSaveClone.removeAll(intersection);
            if (CollectionUtils.isNotEmpty(unSaveClone)){
                unSaveClone.stream()
                        .map(e -> RolePrivilegePO.builder().roleId(Long.valueOf(roleId)).status(Boolean.TRUE).privilegeId(e).build())
                        .collect(Collectors.toSet())
                        .forEach(rolePrivilegeMapper::insert);;
            }

        }else {
            //两个集合不相交（全部删除再全部插入）

            savedClone.forEach(e->rolePrivilegeMapper
                    .delete(Wrappers.<RolePrivilegePO>lambdaQuery()
                            .eq(RolePrivilegePO::getPrivilegeId,e)
                            .eq(RolePrivilegePO::getRoleId,roleId)));

            unSaved.stream()
                    .map(e -> RolePrivilegePO.builder().roleId(Long.valueOf(roleId)).status(Boolean.TRUE).privilegeId(e).build())
                    .collect(Collectors.toSet())
                    .forEach(rolePrivilegeMapper::insert);
        }
    }
}
