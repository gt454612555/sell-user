package sell.user.services.service.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import sell.user.persistence.dto.req.UserSearchDTO;
import sell.user.persistence.po.UserPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/21
 */
public interface UserQueryService extends IService<UserPO> {

    Page<UserPO> getCustomerPage(UserSearchDTO search);

    Page<UserPO> getSellerPage(UserSearchDTO search);

}
