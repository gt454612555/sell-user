package sell.user.services.service.query.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sell.user.persistence.dto.req.RoleSearchDTO;
import sell.user.persistence.mapper.RoleMapper;
import sell.user.persistence.po.RolePO;
import sell.user.services.service.query.RoleQueryService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Service
public class RoleQueryServiceImpl extends ServiceImpl<RoleMapper, RolePO> implements RoleQueryService {
    @Override
    public Page<RolePO> getRolePage(RoleSearchDTO search) {
        return search.setRecords(this.baseMapper.getRolePageList(search));
    }
}
