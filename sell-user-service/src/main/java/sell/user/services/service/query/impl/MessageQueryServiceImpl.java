package sell.user.services.service.query.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sell.common.enums.message.BroadcastObjEnum;
import sell.common.enums.message.MessageTypeEnum;
import sell.common.enums.user.UserTypeEnum;
import sell.common.exception.ServiceException;
import sell.common.security.context.UserContextHolder;
import sell.common.security.dto.UserInfo;
import sell.user.persistence.dto.req.MessageSelectDTO;
import sell.user.persistence.dto.res.MessagePageResDTO;
import sell.user.persistence.mapper.MessageQueryMapper;
import sell.user.services.service.query.MessageQueryService;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Service
@RequiredArgsConstructor
public class MessageQueryServiceImpl extends ServiceImpl<MessageQueryMapper, MessagePageResDTO> implements MessageQueryService {

    private static final Set<MessageTypeEnum> SELLER_TYPE;
    private static final Set<MessageTypeEnum> CUSTOMER_TYPE;
    private static final Set<MessageTypeEnum> ADMIN_TYPE;
    private static final Map<UserTypeEnum,Set<MessageTypeEnum>> ROLE_TYPE_MAP;

    static {
        SELLER_TYPE = new HashSet<>();
        CUSTOMER_TYPE = new HashSet<>();
        ADMIN_TYPE = new HashSet<>();
        ADMIN_TYPE.add(MessageTypeEnum.WITHDRAW_AUDIT);
        ADMIN_TYPE.add(MessageTypeEnum.GOODS_AUDIT);
        ADMIN_TYPE.add(MessageTypeEnum.ORDER_NOTICE);
        ADMIN_TYPE.add(MessageTypeEnum.SYSTEM);

        SELLER_TYPE.add(MessageTypeEnum.ORDER_NOTICE);
        SELLER_TYPE.add(MessageTypeEnum.ACCOUNT_NOTICE);
        SELLER_TYPE.add(MessageTypeEnum.GOODS_NOTICE);
        SELLER_TYPE.add(MessageTypeEnum.SYSTEM);

        CUSTOMER_TYPE.add(MessageTypeEnum.ORDER_NOTICE);
        CUSTOMER_TYPE.add(MessageTypeEnum.SYSTEM);

        ROLE_TYPE_MAP = new HashMap<>();
        ROLE_TYPE_MAP.put(UserTypeEnum.CUSTOMER,CUSTOMER_TYPE);
        ROLE_TYPE_MAP.put(UserTypeEnum.ADMIN,ADMIN_TYPE);
        ROLE_TYPE_MAP.put(UserTypeEnum.SELLER,SELLER_TYPE);

    }

    @Override
    public Page<MessagePageResDTO> getMsgPage(MessageSelectDTO select) {
        @NotNull MessageTypeEnum messageType = select.getType();
        UserInfo userInfo = UserContextHolder.get();
        UserTypeEnum role = userInfo.getRole();
        Set<MessageTypeEnum> myMsgTypes = ROLE_TYPE_MAP.get(role);
        if (!myMsgTypes.contains(messageType)){
            throw new ServiceException("消息类型选择有误");
        }
        return select.setRecords(this.baseMapper.getPageList(select, userInfo.getId(), BroadcastObjEnum.fromValue(role.getValue())));
    }

    @Override
    public Page<MessagePageResDTO> getMyAppMsg(Page<MessagePageResDTO> query) {
        Long userId = UserContextHolder.getUserId();
        return query.setRecords(this.baseMapper.getAppPage(query,userId));
    }

    @Override
    public Integer getUnReadMsgNum() {
        UserInfo userInfo = UserContextHolder.get();
        UserTypeEnum role = userInfo.getRole();
        Long uid = userInfo.getId();
        return this.baseMapper.getUnReadMsgNum(uid, BroadcastObjEnum.fromValue(role.getValue()));
    }
}
