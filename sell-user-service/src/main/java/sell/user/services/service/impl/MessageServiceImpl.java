package sell.user.services.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sell.common.api.dto.SendMsgDTO;
import sell.common.enums.message.BroadcastObjEnum;
import sell.user.persistence.repository.MessageRepository;
import sell.user.services.service.MessageService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    public Long sendSysMsg(String content, BroadcastObjEnum broadcastObjEnum) {

        return messageRepository.sendSysMsg(content, broadcastObjEnum);

    }

    @Override
    public void readMsg(Long msgId) {
        messageRepository.readMsg(msgId);
    }

    @Override
    public void readAll() {
        messageRepository.readAll();
    }
}
