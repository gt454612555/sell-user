package sell.user.services.service;

import sell.common.enums.message.BroadcastObjEnum;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
public interface MessageService {
    /**
     * 发送系统消息
     * @param content 消息内容
     * @param broadcastObjEnum 广播对象
     * @return 消息id
     */
    Long sendSysMsg(String content, BroadcastObjEnum broadcastObjEnum);

    /**
     * 已读一条消息
     * @param msgId
     */
    void readMsg(Long msgId);

    /**
     * 全部消息已读
     */
    void readAll();

}
