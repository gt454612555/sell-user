package sell.user.services.service.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import sell.user.persistence.dto.req.AdminListSearchDTO;
import sell.user.persistence.dto.res.AdminListResDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface AdminQueryService extends IService<AdminListResDTO> {

    Page<AdminListResDTO> getAdminList(AdminListSearchDTO search);

}
