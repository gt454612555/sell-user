package sell.user.services.service.query.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sell.user.persistence.dto.req.UserSearchDTO;
import sell.user.persistence.mapper.UserMapper;
import sell.user.persistence.po.UserPO;
import sell.user.services.service.query.UserQueryService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/21
 */
@Service
public class UserQueryServiceImpl extends ServiceImpl<UserMapper, UserPO> implements UserQueryService {
    @Override
    public Page<UserPO> getCustomerPage(UserSearchDTO search) {
        return search.setRecords(this.baseMapper.getCustomerPage(search));
    }

    @Override
    public Page<UserPO> getSellerPage(UserSearchDTO search) {
        return search.setRecords(this.baseMapper.getSellerPage(search));
    }
}
