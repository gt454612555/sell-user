package sell.user.services.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sell.user.persistence.repository.AdminRepository;
import sell.user.services.dto.UserDTO;
import sell.user.services.dto.req.AdminBaseInfoDTO;
import sell.user.services.factory.UserFactory;
import sell.user.services.service.AdminService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;
    private final UserFactory userFactory;

    @Override
    public Boolean changeIsLock(Boolean isLock, Long id) {
        return adminRepository.changeIsLock(isLock, id);
    }

    @Override
    public Long addAdmin(UserDTO userDTO) {
        return adminRepository.addAdmin(userFactory.toPo(userDTO));
    }

    @Override
    public void deleteAdmin(Long id) {
        adminRepository.deleteAdmin(id);
    }

    @Override
    public void changeAdminBaseInfo(AdminBaseInfoDTO adminBaseInfoDTO) {
        adminRepository.updateAdminBaseInfo(adminBaseInfoDTO.getId(),adminBaseInfoDTO.getMobile(),adminBaseInfoDTO.getNickName(),adminBaseInfoDTO.getSex());
    }
}
