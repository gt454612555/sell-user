package sell.user.services.service.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import sell.user.persistence.dto.req.RoleSearchDTO;
import sell.user.persistence.po.RolePO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface RoleQueryService extends IService<RolePO> {

    Page<RolePO> getRolePage(RoleSearchDTO search);

}
