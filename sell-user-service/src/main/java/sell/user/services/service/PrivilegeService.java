package sell.user.services.service;

import sell.user.services.dto.req.PrivilegeChangeInfoDTO;
import sell.user.services.dto.req.SetPrivilegeDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface PrivilegeService {

    void changePrivilegeInfo(PrivilegeChangeInfoDTO dto);

    void setPrivilege(SetPrivilegeDTO dto);
}
