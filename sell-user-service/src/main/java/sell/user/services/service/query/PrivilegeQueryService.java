package sell.user.services.service.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import sell.common.dal.po.AllPrivilegeResPO;
import sell.common.dal.po.PrivilegePO;
import sell.common.dal.vo.PrivilegeSearchVO;
import sell.user.persistence.dto.res.RolePrivilegeResDTO;
import sell.user.services.dto.res.PrivilegeClassNameResDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface PrivilegeQueryService extends IService<PrivilegePO> {

    Page<PrivilegePO> getPrivilegePage(PrivilegeSearchVO search);

    List<PrivilegeClassNameResDTO> getAllPrivilegeClass();


    List<AllPrivilegeResPO> getAllPrivilege();

    List<RolePrivilegeResDTO> getPrivilegeByRole(Integer roleId);


}
