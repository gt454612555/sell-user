package sell.user.services.service;

import sell.user.services.dto.UserDTO;
import sell.user.services.dto.req.AdminBaseInfoDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
public interface AdminService {

    Boolean changeIsLock(Boolean isLock,Long id);

    Long addAdmin(UserDTO userDTO);

    void deleteAdmin(Long id);

    void changeAdminBaseInfo(AdminBaseInfoDTO adminBaseInfoDTO);
}
