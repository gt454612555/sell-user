package sell.user.services.service.query.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sell.common.dal.mapper.PrivilegeMapper;
import sell.common.dal.po.AllPrivilegeResPO;
import sell.common.dal.po.PrivilegePO;
import sell.common.dal.vo.PrivilegeSearchVO;
import sell.user.persistence.dto.res.RolePrivilegeResDTO;
import sell.user.persistence.mapper.RolePrivilegeMapper;
import sell.user.services.dto.res.PrivilegeClassNameResDTO;
import sell.user.services.service.query.PrivilegeQueryService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Service
@RequiredArgsConstructor
public class PrivilegeQueryServiceImpl extends ServiceImpl<PrivilegeMapper, PrivilegePO> implements PrivilegeQueryService {

    private final PrivilegeMapper privilegeMapper;
    private final RolePrivilegeMapper rolePrivilegeMapper;

    @Override
    public Page<PrivilegePO> getPrivilegePage(PrivilegeSearchVO search) {
        return search.setRecords(this.baseMapper.getPrivilegePage(search));
    }

    @Override
    public List<PrivilegeClassNameResDTO> getAllPrivilegeClass() {

        List<PrivilegePO> allPrivileges = privilegeMapper.selectList(null);

        Map<String, List<PrivilegePO>> collect = allPrivileges.stream().collect(Collectors.groupingBy(PrivilegePO::getName));

        List<String> nameList = new ArrayList<>(collect.keySet());

        List<PrivilegeClassNameResDTO> resList = new ArrayList<>();

        for (int i = 0; i < nameList.size(); i++) {
            resList.add(PrivilegeClassNameResDTO.builder()
                    .id(i + 1)
                    .name(nameList.get(i))
                    .build());
        }

        return resList;
    }


    @Override
    public List<AllPrivilegeResPO> getAllPrivilege() {
        return privilegeMapper.getAllPrivilege();
    }

    @Override
    public List<RolePrivilegeResDTO> getPrivilegeByRole(Integer roleId) {
        return rolePrivilegeMapper.getPrivilegeByRole(roleId);
    }


}
