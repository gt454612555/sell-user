package sell.user.services.service.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import sell.user.persistence.dto.req.MessageSelectDTO;
import sell.user.persistence.dto.res.MessagePageResDTO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/23
 */
public interface MessageQueryService extends IService<MessagePageResDTO> {

    Page<MessagePageResDTO> getMsgPage(MessageSelectDTO select);

    Page<MessagePageResDTO> getMyAppMsg(Page<MessagePageResDTO> query);

    Integer getUnReadMsgNum();

}
