package sell.user.services.service.query.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import sell.user.persistence.dto.req.AdminListSearchDTO;
import sell.user.persistence.dto.res.AdminListResDTO;
import sell.user.persistence.mapper.AdminListQueryMapper;
import sell.user.services.service.query.AdminQueryService;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Service
public class AdminQueryServiceImpl extends ServiceImpl<AdminListQueryMapper, AdminListResDTO> implements AdminQueryService {
    @Override
    public Page<AdminListResDTO> getAdminList(AdminListSearchDTO search) {
        return search.setRecords(this.baseMapper.getAdminList(search));
    }
}
