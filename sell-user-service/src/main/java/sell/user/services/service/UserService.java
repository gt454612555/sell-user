package sell.user.services.service;

import sell.common.api.dto.CustomerAddrDTO;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.services.dto.UserDTO;
import sell.user.services.dto.res.CustomerAddrResDTO;
import sell.user.services.dto.res.CustomerInfoAddrResDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
public interface UserService {

    /**
     * 用户注册
     * @param user 用户dto
     * @return 返回token
     */
    Map<String,Object> userRegister(UserDTO user);

    Map<String,Object> registerAdmin(UserDTO user);

    /**
     * 修改用户密码
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return 返回token
     */
    Map<String,Object> changePasswordByOld(String oldPassword,String newPassword);

    /**
     * 设置用户信息（新增/修改）
     * @param po
     * @return
     */
    Boolean setExtraInfo(ExtraInfoPO po);

    /**
     * 新增一个收货地址，如果当前没有默认收货地址，就将当前地址设为默认地址
     * @param po
     * @return 主键
     */
    Long addAddr(CustomerAddrPO po);

    /**
     * 删除一个收货地址，无法删除默认收货地址
     * @param addrId
     * @return
     */
    Boolean deleteAddr(Long addrId);



    /**
     * 获取用户的额外信息
     * @return
     */
    ExtraInfoPO getExtraInfo();

    /**
     * 获取收货地址信息+数据权限校验
     * @param uid
     * @param addrId
     * @return
     */
    CustomerAddrDTO getAndCheckAddrInfo(Long uid,Long addrId);

    /**
     * 获取默认收货地址
     * @param uid
     * @return
     */
    CustomerAddrDTO getDefaultAddr(Long uid);

    /**
     * 重置密码
     * @param id
     */
    void resetPassword(Long id);

    /**
     * 删除用户
     * @param id
     */
    void deleteUser(Long id);

    /**
     * 查询用户的购物车数量
     * @return
     */
    Integer getCartNum();

    /**
     * 修改购物车的购买数量
     * @param id
     * @param num
     */
    void changeCartBuyNum(Long id,Integer num);

    List<CustomerAddrResDTO> getAddrList();

    /**
     * 将收货地址设置为默认收货地址
     * @param addrId
     */
    void setDefaultAddr(Long addrId);

    /**
     * 获取账户余额
     * @return
     */
    BigDecimal getBalance();

    /**
     * 更换头像
     * @param avatar
     */
    void changeAvatar(Long avatar);
}
