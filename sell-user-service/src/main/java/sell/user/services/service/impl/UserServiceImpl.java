package sell.user.services.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sell.common.api.dto.CustomerAddrDTO;
import sell.common.api.dto.SendMsgDTO;
import sell.common.api.rpc.AccountRpcFacade;
import sell.common.api.rpc.AreaRpcFacade;
import sell.common.api.rpc.FileRpcFacade;
import sell.common.enums.message.MessageTemplateEnum;
import sell.common.enums.message.MessageTypeEnum;
import sell.common.enums.message.SendTypeEnum;
import sell.common.enums.user.UserTypeEnum;
import sell.common.exception.ServiceException;
import sell.common.security.context.UserContextHolder;
import sell.common.security.dto.UserInfo;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.persistence.po.UserPO;
import sell.user.persistence.repository.MessageRepository;
import sell.user.services.dto.UserDTO;
import sell.user.services.dto.res.CustomerAddrResDTO;
import sell.user.services.dto.res.CustomerInfoAddrResDTO;
import sell.user.services.factory.CustomerAddrFactory;
import sell.user.services.factory.UserFactory;
import sell.user.persistence.repository.UserRepository;
import sell.user.services.service.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static sell.common.security.utils.JwtUtils.flushToken;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    @Reference(check = false)
    private AreaRpcFacade areaRpcFacade;
    @Reference(check = false)
    private AccountRpcFacade accountRpcFacade;
    @Reference(check = false)
    private FileRpcFacade fileRpcFacade;

    private final UserRepository userRepository;
    private final UserFactory userFactory;
    private final CustomerAddrFactory customerAddrFactory;
    private final MessageRepository messageRepository;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> userRegister(UserDTO user) {
        //校验
        Set<UserTypeEnum> roleSet = new HashSet<>(2);
        roleSet.add(UserTypeEnum.CUSTOMER);
        roleSet.add(UserTypeEnum.SELLER);
        user.setIsLock(Boolean.FALSE);
        if (!roleSet.contains(user.getRole())) {
            throw new ServiceException("您注册的用户类型有误！");
        }
        //检查手机号重复和昵称重复
        if (userRepository.hasSameNickNameAndMobile(user.getNickName(), user.getMobile()).get("mobile")) {
            throw new ServiceException("手机号已存在，请换其他手机号");
        }
        //加密密码
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(user.getPassword());
        user.setPassword(encode);
        //开始注册，向User表和UserRole表插入数据
        Long userId = userRepository.insertIntoUserAndRole(userFactory.toPo(user));
        UserInfo userInfo = userFactory.dtoToUserInfo(user);
        //生成账户信息
        accountRpcFacade.initAccount(userId);
        userInfo.setId(userId);
        //生成token返回给前端
        Map<String, Object> flushToken = flushToken(userInfo, null);
        //发送消息
        Map<String,Object> params = new HashMap<>();
        params.put("mobile",user.getMobile());
        SendMsgDTO msgDTO = SendMsgDTO.builder()
                .broadcastObj(null)
                .messageTemplate(MessageTemplateEnum.REGISTER)
                .params(params)
                .recUid(userId)
                .sendType(SendTypeEnum.SINGLE)
                .type(MessageTypeEnum.ACCOUNT_NOTICE)
                .build();
        messageRepository.sendMsg(msgDTO);
        return flushToken;
    }

    @Override
    public Map<String, Object> registerAdmin(UserDTO user) {
        user.setRole(UserTypeEnum.ADMIN);
        //检查手机号重复和昵称重复
        Map<String, Boolean> hasSame = userRepository.hasSameNickNameAndMobile(user.getNickName(), user.getMobile());
        Boolean hasSameMobile = hasSame.get("mobile");
        Boolean hasSameNickName = hasSame.get("nickName");
        if (hasSameMobile) {
            throw new ServiceException("手机号已存在，请换其他手机号");
        }
        user.setIsLock(Boolean.FALSE);
        //加密密码
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(user.getPassword());
        user.setPassword(encode);
        //开始注册，向User表和UserRole表插入数据
        Long userId = userRepository.insertIntoUserAndRole(userFactory.toPo(user));
        UserInfo userInfo = userFactory.dtoToUserInfo(user);
        //生成账户信息
        accountRpcFacade.initAccount(userId);
        userInfo.setId(userId);
        //生成token返回给前端
        return flushToken(userInfo, null);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> changePasswordByOld(String oldPassword, String newPassword) {
        UserInfo oldUserInfo = UserContextHolder.get();
        Long userId = oldUserInfo.getId();

        //比对旧密码
        UserPO user = userRepository.getUserById(userId);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean hasMatchOldPassword = encoder.matches(oldPassword, user.getPassword());
        if (hasMatchOldPassword) {
            if (oldPassword.equals(newPassword)) {
                throw new ServiceException("新密码不能与原密码相同！");
            }
            if (encoder.matches(newPassword, user.getPassword())) {
                throw new ServiceException("新密码不能与原密码相同！");
            }
            //加密新密码
            String encodedPassword = encoder.encode(newPassword);
            user.setPassword(encodedPassword);

            userRepository.updateUserInfo(user);
            UserInfo newUserInfo = userFactory.poToUserInfo(user);
            //刷新token
            return flushToken(newUserInfo, oldUserInfo);
        } else {
            throw new ServiceException("原密码错误！");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean setExtraInfo(ExtraInfoPO po) {
        UserInfo userInfo = UserContextHolder.get();
        if (UserTypeEnum.CUSTOMER.equals(userInfo.getRole())){
            if (StringUtils.isNotBlank(po.getEntityName())) {
                throw new ServiceException("顾客用户不能拥有主体信息！");
            }
        }else if (UserTypeEnum.SELLER.equals(userInfo.getRole())){
            if (StringUtils.isBlank(po.getEntityName())){
                throw new ServiceException("请填写主体信息！");
            }
        }
        Long userId = userInfo.getId();
        po.setUid(userId);
        //查询数据库是否已有信息
        ExtraInfoPO extraInfo = userRepository.getExtraInfo(userId);
        if (extraInfo==null){
            //插入
            Long id = userRepository.insertExtraInfo(po);
            return id!=null;
        }else {
            //更新
            return userRepository.updateExtraInfo(po);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long addAddr(CustomerAddrPO po) {
        Long userId = UserContextHolder.getUserId();
        po.setUid(userId);
        //查询是否有默认收货地址
        CustomerAddrPO defaultAddr = userRepository.getDefaultAddr(userId);
        if (defaultAddr==null){
            po.setIsDefault(Boolean.TRUE);
        }else {
            po.setIsDefault(Boolean.FALSE);
        }
        //填充省市区名称
        String provinceCode = po.getProvinceCode();
        String cityCode = po.getCityCode();
        String areaCode = po.getAreaCode();
        po.setAreaName(StringUtils.isBlank(areaCode)?"":areaRpcFacade.getNameByCode(areaCode));
        po.setProvinceName(areaRpcFacade.getNameByCode(provinceCode));
        po.setCityName(areaRpcFacade.getNameByCode(cityCode));
        return userRepository.insertCustomerAddr(po);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteAddr(Long addrId) {
        Long userId = UserContextHolder.getUserId();
        CustomerAddrPO addr = userRepository.getAddrById(addrId);
        if (addr==null){
            throw new ServiceException("您要删除的收货地址不存在！");
        }
        Long uid = addr.getUid();
        if (!userId.equals(uid)){
            throw new ServiceException("您无权删除他人的收货地址！");
        }
        if (addr.getIsDefault()){
            throw new ServiceException("默认收货地址无法删除！");
        }
        //删除
        return userRepository.deleteAddr(addrId);
    }



    @Override
    public ExtraInfoPO getExtraInfo() {
        Long userId = UserContextHolder.getUserId();
        return userRepository.getExtraInfo(userId);
    }

    @Override
    public CustomerAddrDTO getAndCheckAddrInfo(Long uid, Long addrId) {
        CustomerAddrPO addrPO = userRepository.getAddrInfo(uid, addrId);
        return customerAddrFactory.poToRpcDTO(addrPO);
    }

    @Override
    public CustomerAddrDTO getDefaultAddr(Long uid) {
        CustomerAddrPO defaultAddr = userRepository.getDefaultAddr(uid);
        return customerAddrFactory.poToRpcDTO(defaultAddr);
    }

    @Override
    public void resetPassword(Long id) {
        UserPO user = userRepository.getUserById(id);
        if (ObjectUtils.isEmpty(user)){
            throw new ServiceException("用户不存在");
        }
        UserPO clone = new UserPO();
        BeanUtils.copyProperties(user,clone);
        String newPassword = "123456";
        String encode = new BCryptPasswordEncoder().encode(newPassword);
        user.setPassword(encode);
        userRepository.updateUserInfo(user);
        //刷新token
        UserInfo newUserInfo = userFactory.poToUserInfo(user);
        UserInfo oldUserInfo = userFactory.poToUserInfo(clone);
        //刷新token
        flushToken(newUserInfo, oldUserInfo);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteUser(id);
    }

    @Override
    public Integer getCartNum() {
        return userRepository.getCartNum(UserContextHolder.getUserId());
    }

    @Override
    public void changeCartBuyNum(Long id, Integer num) {
        userRepository.changeCartNum(id, num);
    }

    @Override
    public List<CustomerAddrResDTO> getAddrList() {
        Long userId = UserContextHolder.getUserId();
        List<CustomerAddrPO> addrList = userRepository.getAddrList(userId);
        return userFactory.addrPosToDtoList(addrList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setDefaultAddr(Long addrId) {
        Long userId = UserContextHolder.getUserId();
        List<CustomerAddrPO> addrList = userRepository.getAddrList(userId);
        if (CollectionUtils.isNotEmpty(addrList)){
            List<Long> addrIdList = addrList.stream().map(CustomerAddrPO::getId).collect(Collectors.toList());
            if (!addrIdList.contains(addrId)){
                throw new ServiceException("收货地址信息有误");
            }
            //设置默认收货地址
            Long defaultAddrId = addrList.stream().filter(CustomerAddrPO::getIsDefault).map(CustomerAddrPO::getId).findFirst().orElse(null);
            Assert.notNull(defaultAddrId,"默认地址数据有误！");
            userRepository.setDefaultAddrId(defaultAddrId,addrId);
        }else {
            throw new ServiceException("请先至少设置一个收货地址！");
        }
    }

    @Override
    public BigDecimal getBalance() {
        return accountRpcFacade.getBalance(UserContextHolder.getUserId());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changeAvatar(Long avatar) {
        Long userId = UserContextHolder.getUserId();
        ExtraInfoPO extraInfo = userRepository.getExtraInfo(userId);
        if (extraInfo == null){
            //直接添加
            ExtraInfoPO extraInfoPO = ExtraInfoPO.builder()
                    .uid(userId)
                    .avatar(avatar)
                    .build();
            userRepository.insertExtraInfo(extraInfoPO);
        }else {
            Long oldAvatar = extraInfo.getAvatar();
            fileRpcFacade.deleteFile(oldAvatar);
            //更新头像
            userRepository.updateAvatar(avatar);
        }

    }
}
