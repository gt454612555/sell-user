package sell.user.services.factory;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import sell.common.api.dto.CustomerAddrDTO;
import sell.user.persistence.po.CustomerAddrPO;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/3/4
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface CustomerAddrFactory {

    CustomerAddrDTO poToRpcDTO(CustomerAddrPO po);

}
