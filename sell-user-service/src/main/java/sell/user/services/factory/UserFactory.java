package sell.user.services.factory;

import org.mapstruct.*;
import sell.common.security.dto.UserInfo;
import sell.user.persistence.po.CustomerAddrPO;
import sell.user.persistence.po.ExtraInfoPO;
import sell.user.services.dto.UserDTO;
import sell.user.persistence.po.UserPO;
import sell.user.services.dto.res.CustomerAddrResDTO;
import sell.user.services.dto.res.CustomerInfoAddrResDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Mapper(
        builder = @Builder(disableBuilder = true),
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserFactory {
    /**
     * dto 2 po
     * @param dto
     * @return
     */
    UserPO toPo(UserDTO dto);

    /**
     * po 2 userInfo
     * @param userPO
     * @return
     */
    UserInfo poToUserInfo(UserPO userPO);

    /**
     * dto 2 userInfo
     * @param userDTO
     * @return
     */
    UserInfo dtoToUserInfo(UserDTO userDTO);

    CustomerAddrResDTO addrPoToDto(CustomerAddrPO po);

    List<CustomerAddrResDTO> addrPosToDtoList(List<CustomerAddrPO> poList);

    @Mappings({
            @Mapping(target = "addrList",source = "addrList")
    })
    CustomerInfoAddrResDTO toInfoAndAddrDto(ExtraInfoPO infoPo, List<CustomerAddrPO> addrList);
}
