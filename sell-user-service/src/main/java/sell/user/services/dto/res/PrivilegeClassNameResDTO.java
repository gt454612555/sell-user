package sell.user.services.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "权限模块分类 返回参数")
public class PrivilegeClassNameResDTO {

    @ApiModelProperty(value = "索引")
    private Integer id;

    @ApiModelProperty(value = "名字")
    private String name;

}
