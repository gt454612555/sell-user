package sell.user.services.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("权限信息修改 -- dto入参对象")
public class PrivilegeChangeInfoDTO {

    @ApiModelProperty(value = "权限id")
    @NotNull
    private Long id;

    @ApiModelProperty(value = "权限模块名称")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "权限描述")
    @NotBlank
    private String description;

}
