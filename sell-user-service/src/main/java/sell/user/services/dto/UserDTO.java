package sell.user.services.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sell.common.enums.user.SexTypeEnum;
import sell.common.enums.user.UserTypeEnum;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/8
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户dto对象")
public class UserDTO {

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "性别")
    private SexTypeEnum sex;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "用户类型")
    private UserTypeEnum role;

    @ApiModelProperty(value = "状态")
    private Boolean isLock;
}
