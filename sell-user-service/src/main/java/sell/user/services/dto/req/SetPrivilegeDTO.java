package sell.user.services.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/4/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetPrivilegeDTO {

    @ApiModelProperty(value = "角色ID")
    @NotNull
    private Integer roleId;

    @ApiModelProperty(value = "权限id列表")
    @NotNull
    private List<Long> privilegeIds;

}
