package sell.user.services.dto.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author gt
 * @date 2021/1/30
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户信息页面查询 返回参数")
public class CustomerInfoAddrResDTO {

    @ApiModelProperty(value = "当前用户id")
    private Long uid;

    @ApiModelProperty(value = "真实姓名",required = true)
    private String realName;

    @ApiModelProperty(value = "邮箱",required = true)
    private String email;

    @ApiModelProperty(value = "年龄",required = true)
    private Integer age;

    @ApiModelProperty(value = "支付宝账户--退款专用",required = true)
    private String aliPay;

    @ApiModelProperty(value = "头像id")
    private Long avatar;

    @ApiModelProperty(value = "收货地址信息列表",required = true)
    private List<CustomerAddrResDTO> addrList;

}
